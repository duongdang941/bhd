<?php


namespace KodKodKod\Catalog\Plugin\Magento\Catalog\Controller\Adminhtml\Product\Action\Attribute;

use Magento\Catalog\Controller\Adminhtml\Product\Action\Attribute\Save;
use ShoppingFeed\Manager\Model\ResourceModel\Feed\ProductFactory as FeedProductResourceFactory;
use Magento\Catalog\Helper\Product\Edit\Action\Attribute;

class SavePlugin
{
    /**
     * @var FeedProductResourceFactory
     */
    private $feedProductResourceFactory;
    /**
     * @var Attribute
     */
    private $attributeHelper;

    public function __construct(
        FeedProductResourceFactory $feedProductResourceFactory,
        Attribute                  $attributeHelper
    )
    {
        $this->feedProductResourceFactory = $feedProductResourceFactory;
        $this->attributeHelper = $attributeHelper;
    }

    /**
     * @param Save $subject
     */
    public function beforeExecute(Save $subject)
    {
        $productIds = $this->attributeHelper->getProductIds();
        if (empty($productIds)) {
            return;
        }
        $feedAttributes = $subject->getRequest()->getParam('sfm_feed_attributes');
        if (empty($feedAttributes)) {
            return;
        }
        $feedProductResource = $this->feedProductResourceFactory->create();

            foreach ($feedAttributes as $storeId => $storeFeedAttributes) {
                $feedProductResource->updateProductFeedAttributes(
                    $productIds,
                    (int) $storeId,
                    $storeFeedAttributes['is_selected'] ?? false,
                    $storeFeedAttributes['selected_category_id'] ?? null
                );
            }
    }

}
