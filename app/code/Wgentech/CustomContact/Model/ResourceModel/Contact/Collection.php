<?php

namespace Wgentech\CustomContact\Model\ResourceModel\Contact;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Wgentech\CustomContact\Model\ResourceModel\Contact as ResourceModel;
use Wgentech\CustomContact\Model\Contact as Model;

class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_eventPrefix = 'wgentech_contact_collection';

    /**
     * @inheritdoc
     */
    protected function _construct()
    {
        $this->_init(Model::class, ResourceModel::class);
    }
}
