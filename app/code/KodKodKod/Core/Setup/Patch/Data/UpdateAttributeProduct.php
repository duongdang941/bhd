<?php

declare(strict_types=1);

namespace KodKodKod\Core\Setup\Patch\Data;

use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;
use Magento\Eav\Model\ResourceModel\Entity\Attribute;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;

/**
 * Class UpdateProduct
 *
 * @author    Agence KodKodKod
 * @copyright 2021 KodKodKod
 * @link      https://kodkodkod.studio
 */
class UpdateAttributeProduct implements DataPatchInterface
{
	/**
	 * @var EavSetup
	 */
	protected EavSetupFactory $eavSetupFactory;
	/**
	 * @var Attribute
	 */
	protected Attribute $eavAttribute;

	/**
	 * @var ModuleDataSetupInterface
	 */
	private $moduleDataSetup;


	/**
	 * UpdateAddressLabelRequired constructor.
	 * @param EavSetup $eavSetupFactory
	 * @param Attribute $eavAttribute
	 */
	public function __construct(
		ModuleDataSetupInterface $moduleDataSetup,
		EavSetupFactory $eavSetupFactory,
		Attribute $eavAttribute
	) {
		$this->eavSetupFactory = $eavSetupFactory;
		$this->eavAttribute = $eavAttribute;
		$this->moduleDataSetup = $moduleDataSetup;
	}

	/**
	 * update required value true to false for address_label attribute
	 *
	 * @return void
	 */
	public function apply(): void
	{

		$this->moduleDataSetup->getConnection()->startSetup();
		/** @var EavSetupFactory $eavSetup */
		$eavSetup = $this->eavSetupFactory->create(['setup' => $this->moduleDataSetup]);

		$eavSetup->addAttribute(
			\Magento\Catalog\Model\Product::ENTITY,
			'occultation',
			[
				'group' => 'General',
				'type' => 'int',
				'backend' => '',
				'frontend' => '',
				'label' => 'Occultation optimale',
				'input' => 'boolean',
				'class' => '',
				'source' => \Magento\Eav\Model\Entity\Attribute\Source\Boolean::class,
				'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
				'visible' => true,
				'required' => false,
				'user_defined' => false,
				'default' => '1',
				'searchable' => false,
				'filterable' => false,
				'comparable' => false,
				'visible_on_front' => false,
				'used_in_product_listing' => false,
				'unique' => false,
				'apply_to' => 'simple,configurable,virtual,bundle,downloadable'
			]
		);


		$eavSetup->addAttribute(
			\Magento\Catalog\Model\Product::ENTITY,
			'easy_assembly',
			[
				'group' => 'General',
				'type' => 'int',
				'backend' => '',
				'frontend' => '',
				'label' => 'Montage facile',
				'input' => 'boolean',
				'class' => '',
				'source' => \Magento\Eav\Model\Entity\Attribute\Source\Boolean::class,
				'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
				'visible' => true,
				'required' => false,
				'user_defined' => false,
				'default' => '1',
				'searchable' => false,
				'filterable' => false,
				'comparable' => false,
				'visible_on_front' => false,
				'used_in_product_listing' => false,
				'unique' => false,
				'apply_to' => 'simple,configurable,virtual,bundle,downloadable'
			]
		);

		$eavSetup->addAttribute(
			\Magento\Catalog\Model\Product::ENTITY,
			'water_repellent',
			[
				'group' => 'General',
				'type' => 'int',
				'backend' => '',
				'frontend' => '',
				'label' => 'Déperlant',
				'input' => 'boolean',
				'class' => '',
				'source' => \Magento\Eav\Model\Entity\Attribute\Source\Boolean::class,
				'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
				'visible' => true,
				'required' => false,
				'user_defined' => false,
				'default' => '1',
				'searchable' => false,
				'filterable' => false,
				'comparable' => false,
				'visible_on_front' => false,
				'used_in_product_listing' => false,
				'unique' => false,
				'apply_to' => 'simple,configurable,virtual,bundle,downloadable'
			]
		);


		$eavSetup->addAttribute(
			\Magento\Catalog\Model\Product::ENTITY,
			'stretch_fabric',
			[
				'group' => 'General',
				'type' => 'int',
				'backend' => '',
				'frontend' => '',
				'label' => 'Tissu extensible',
				'input' => 'boolean',
				'class' => '',
				'source' => \Magento\Eav\Model\Entity\Attribute\Source\Boolean::class,
				'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
				'visible' => true,
				'required' => false,
				'user_defined' => false,
				'default' => '1',
				'searchable' => false,
				'filterable' => false,
				'comparable' => false,
				'visible_on_front' => false,
				'used_in_product_listing' => false,
				'unique' => false,
				'apply_to' => 'simple,configurable,virtual,bundle,downloadable'
			]
		);

		$eavSetup->addAttribute(
			\Magento\Catalog\Model\Product::ENTITY,
			'form_possible',
			[
				'group' => 'General',
				'type' => 'int',
				'backend' => '',
				'frontend' => '',
				'label' => 'Toute forme possible',
				'input' => 'boolean',
				'class' => '',
				'source' => \Magento\Eav\Model\Entity\Attribute\Source\Boolean::class,
				'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
				'visible' => true,
				'required' => false,
				'user_defined' => false,
				'default' => '1',
				'searchable' => false,
				'filterable' => false,
				'comparable' => false,
				'visible_on_front' => false,
				'used_in_product_listing' => false,
				'unique' => false,
				'apply_to' => 'simple,configurable,virtual,bundle,downloadable'
			]
		);
		$eavSetup->addAttribute(
			\Magento\Catalog\Model\Product::ENTITY,
			'customization_masts_fittings',
			[
				'group' => 'General',
				'type' => 'int',
				'backend' => '',
				'frontend' => '',
				'label' => 'Personnalisation mâts et accastillages',
				'input' => 'boolean',
				'class' => '',
				'source' => \Magento\Eav\Model\Entity\Attribute\Source\Boolean::class,
				'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
				'visible' => true,
				'required' => false,
				'user_defined' => false,
				'default' => '1',
				'searchable' => false,
				'filterable' => false,
				'comparable' => false,
				'visible_on_front' => false,
				'used_in_product_listing' => false,
				'unique' => false,
				'apply_to' => 'simple,configurable,virtual,bundle,downloadable'
			]
		);
		$eavSetup->addAttribute(
			\Magento\Catalog\Model\Product::ENTITY,
			'choice_shape',
			[
				'group' => 'General',
				'type' => 'int',
				'backend' => '',
				'frontend' => '',
				'label' => 'Choix de forme',
				'input' => 'boolean',
				'class' => '',
				'source' => \Magento\Eav\Model\Entity\Attribute\Source\Boolean::class,
				'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
				'visible' => true,
				'required' => false,
				'user_defined' => false,
				'default' => '1',
				'searchable' => false,
				'filterable' => false,
				'comparable' => false,
				'visible_on_front' => false,
				'used_in_product_listing' => false,
				'unique' => false,
				'apply_to' => 'simple,configurable,virtual,bundle,downloadable'
			]
		);
		$eavSetup->addAttribute(
			\Magento\Catalog\Model\Product::ENTITY,
			'range_width',
			[
				'group' => 'General',
				'type' => 'int',
				'backend' => '',
				'frontend' => '',
				'label' => 'Largeur de gamme',
				'input' => 'boolean',
				'class' => '',
				'source' => \Magento\Eav\Model\Entity\Attribute\Source\Boolean::class,
				'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
				'visible' => true,
				'required' => false,
				'user_defined' => false,
				'default' => '1',
				'searchable' => false,
				'filterable' => false,
				'comparable' => false,
				'visible_on_front' => false,
				'used_in_product_listing' => false,
				'unique' => false,
				'apply_to' => 'simple,configurable,virtual,bundle,downloadable'
			]
		);
		$eavSetup->addAttribute(
			\Magento\Catalog\Model\Product::ENTITY,
			'adaptation_shade_space',
			[
				'group' => 'General',
				'type' => 'int',
				'backend' => '',
				'frontend' => '',
				'label' => 'Adaptation à votre espace d’ombrage',
				'input' => 'boolean',
				'class' => '',
				'source' => \Magento\Eav\Model\Entity\Attribute\Source\Boolean::class,
				'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
				'visible' => true,
				'required' => false,
				'user_defined' => false,
				'default' => '1',
				'searchable' => false,
				'filterable' => false,
				'comparable' => false,
				'visible_on_front' => false,
				'used_in_product_listing' => false,
				'unique' => false,
				'apply_to' => 'simple,configurable,virtual,bundle,downloadable'
			]
		);
		$eavSetup->addAttribute(
			\Magento\Catalog\Model\Product::ENTITY,
			'maximum_durability',
			[
				'group' => 'General',
				'type' => 'int',
				'backend' => '',
				'frontend' => '',
				'label' => 'Durabilité maximale',
				'input' => 'boolean',
				'class' => '',
				'source' => \Magento\Eav\Model\Entity\Attribute\Source\Boolean::class,
				'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
				'visible' => true,
				'required' => false,
				'user_defined' => false,
				'default' => '1',
				'searchable' => false,
				'filterable' => false,
				'comparable' => false,
				'visible_on_front' => false,
				'used_in_product_listing' => false,
				'unique' => false,
				'apply_to' => 'simple,configurable,virtual,bundle,downloadable'
			]
		);
		$eavSetup->addAttribute(
			\Magento\Catalog\Model\Product::ENTITY,
			'multiple_finishes',
			[
				'group' => 'General',
				'type' => 'int',
				'backend' => '',
				'frontend' => '',
				'label' => 'Multiples finitions',
				'input' => 'boolean',
				'class' => '',
				'source' => \Magento\Eav\Model\Entity\Attribute\Source\Boolean::class,
				'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
				'visible' => true,
				'required' => false,
				'user_defined' => false,
				'default' => '1',
				'searchable' => false,
				'filterable' => false,
				'comparable' => false,
				'visible_on_front' => false,
				'used_in_product_listing' => false,
				'unique' => false,
				'apply_to' => 'simple,configurable,virtual,bundle,downloadable'
			]
		);

		$eavSetup->addAttribute(
			\Magento\Catalog\Model\Product::ENTITY,
			'color_chart',
			[
				'group' => 'General',
				'type' => 'text',
				'backend' => '',
				'frontend' => '',
				'label' => 'Nuancier',
				'input' => 'textarea',
				'class' => '',
				'source' => '',
				'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
				'visible' => true,
				'required' => false,
				'user_defined' => false,
				'searchable' => false,
				'filterable' => true,
				'comparable' => false,
				'visible_on_front' => true,
				'used_in_product_listing' => true,
				'wysiwyg_enabled' => true,
				'is_html_allowed_on_front' => true,
				'default' => '',
				'unique' => false,
				'apply_to' => 'simple,configurable,virtual,bundle,downloadable'
			]
		);

		$this->moduleDataSetup->getConnection()->endSetup();
	}

	/**
	 * {@inheritdoc}
	 *
	 * @return string[]
	 */
	public static function getDependencies(): array
	{
		return [];
	}

	/**
	 * {@inheritdoc}
	 *
	 * @return string[]
	 */
	public function getAliases(): array
	{
		return [];
	}
}
