<?php

namespace KodKodKod\CustomProductOption\Plugin\Magento\Tax\Model\Sales\Total\Quote;

use Magento\Quote\Api\Data\ShippingAssignmentInterface;
use Magento\Quote\Model\Quote;
use Magento\Quote\Model\Quote\Address\Total;
use Magento\Quote\Model\Quote\Item\AbstractItem;
use Magento\Tax\Api\Data\TaxDetailsItemInterface;
use Magento\Tax\Model\Sales\Total\Quote\CommonTaxCollector;
use Magento\Tax\Model\Sales\Total\Quote\Subtotal;

class SubtotalPlugin extends CommonTaxCollector
{
    /**
     * @param Subtotal $subject
     * @param $result
     * @param Quote $quote
     * @param ShippingAssignmentInterface $shippingAssignment
     * @param Total $total
     */
    public function afterCollect(
        Subtotal                    $subject,
                                    $result,
        Quote                       $quote,
        ShippingAssignmentInterface $shippingAssignment,
        Total                       $total
    )
    {
        $store = $quote->getStore();
        $priceIncludesTax = $this->_config->priceIncludesTax($store);
        $itemDataObjects = $this->mapItems($shippingAssignment, $priceIncludesTax, false);
        $quoteDetails = $this->prepareQuoteDetails($shippingAssignment, $itemDataObjects);
        $taxDetails = $this->taxCalculationService
            ->calculateTax($quoteDetails, $store->getStoreId());

        $itemDataObjects = $this->mapItems($shippingAssignment, $priceIncludesTax, true);
        $baseQuoteDetails = $this->prepareQuoteDetails($shippingAssignment, $itemDataObjects);
        $baseTaxDetails = $this->taxCalculationService
            ->calculateTax($baseQuoteDetails, $store->getStoreId());

        $itemsByType = $result->organizeItemTaxDetailsByType($taxDetails, $baseTaxDetails);

        // check if include tax with custom-options
        if (isset($itemsByType['wgt-custom-options'])) {
            // @see \Magento\Tax\Model\Sales\Total\Quote\CommonTaxCollector::processProductItems
            /** @var AbstractItem[] $keyedAddressItems */
            $keyedAddressItems = [];
            foreach ($shippingAssignment->getItems() as $addressItem) {
                $keyedAddressItems[$addressItem->getTaxCalculationItemId()] = $addressItem;
            }

            foreach ($itemsByType['wgt-custom-options'] as $code => $itemTaxDetail) {
                /** @var TaxDetailsItemInterface $taxDetail */
                $taxDetail = $itemTaxDetail[$result::KEY_ITEM];
                /** @var TaxDetailsItemInterface $baseTaxDetail */
                $baseTaxDetail = $itemTaxDetail[$result::KEY_BASE_ITEM];

                $quoteItem = $keyedAddressItems[str_replace('wgt-custom-options', '', $code)] ?? null;
                if (!$quoteItem) {
                    continue;
                }

                $store = $shippingAssignment->getShipping()->getAddress()->getQuote()->getStore();
                // add product price and custom options price
                $taxDetail->setPrice($taxDetail->getPrice() + $quoteItem->getPrice());
                $baseTaxDetail->setPrice($baseTaxDetail->getPrice() + $quoteItem->getBasePrice());
                $result->updateItemTaxInfo($quoteItem, $taxDetail, $baseTaxDetail, $store);
            }
        }
        return $result;
    }
}
