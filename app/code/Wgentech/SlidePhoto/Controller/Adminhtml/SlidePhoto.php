<?php

namespace Wgentech\SlidePhoto\Controller\Adminhtml;

abstract class SlidePhoto extends \Magento\Backend\App\Action
{
    const ADMIN_RESOURCE = 'Wgentech_SlidePhoto::menu';

    /**
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry
    ) {
        $this->_coreRegistry = $coreRegistry;
        parent::__construct($context);
    }


    public function initPage($resultPage)
    {
        $resultPage->setActiveMenu(self::ADMIN_RESOURCE)
            ->addBreadcrumb(__('Wgentech'), __('Wgentech'))
            ->addBreadcrumb(__('SlidePhoto'), __('SlidePhoto'));
        return $resultPage;
    }
}