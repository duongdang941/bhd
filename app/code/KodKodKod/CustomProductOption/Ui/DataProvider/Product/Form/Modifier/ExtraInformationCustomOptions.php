<?php

namespace KodKodKod\CustomProductOption\Ui\DataProvider\Product\Form\Modifier;


use KodKodKod\CustomProductOption\Model\ResourceModel\ExtraInformationCustomOptions\CollectionFactory;
use Magento\Catalog\Model\Locator\LocatorInterface;
use Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\AbstractModifier;
use Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\CustomOptions as CustomOptionsModifier;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Framework\UrlInterface;
use Magento\Ui\Component\Container;
use Magento\Ui\Component\DynamicRows;
use Magento\Ui\Component\Form\Element\ActionDelete;
use Magento\Ui\Component\Form\Element\DataType\Text;
use Magento\Ui\Component\Form\Element\Input;
use Magento\Ui\Component\Form\Element\Textarea;
use Magento\Ui\Component\Form\Field;
use Magento\Ui\Component\Form\Fieldset;
use Magento\Ui\Component\Modal;
use Magento\Framework\Stdlib\ArrayManager;
/**
 * KodKodKod\CustomProductOption\Ui\DataProvider\Product\Form\Modifier
 */
class ExtraInformationCustomOptions extends AbstractModifier
{
    /**
     * Group values
     */
    const GROUP_VALUES_OPTIONS_NAME = 'extra_information_group_values_options';

    /**
     * Grid values
     */
    const GRID_VALUES_OPTIONS_NAME = 'extra_information_values_options';
    /**
     *
     */
    const GRID_TYPE_SELECT_NAME = 'values';

    /**
     * Button values
     */
    const BUTTON_ADD = 'button_add';

    /**
     * Field values
     */
    const FIELD_IS_DELETE = 'is_delete';

    /**
     * Import options values
     */
    const CUSTOM_OPTIONS_LISTING = 'product_extra_information_options_listing';

    /**
     *
     */
    const IMPORT_OPTIONS_MODAL = 'import_extra_information_options_modal';

    /**
     * Container values
     */
    const CONTAINER_HEADER_NAME = 'extra_information_container_header';
    /**
     *
     */
    const CONTAINER_OPTION = 'extra_information_container_option';
    /**
     *
     */
    const CONTAINER_COMMON_NAME = 'extra_information_container_common';

    /**
     *
     */
    const FIELD_OPTION_ID = 'option_id';

    /**
     * @var
     */
    protected $meta;


    /**
     * @var SerializerInterface
     */
    protected SerializerInterface $serialize;
    /**
     * @var CollectionFactory
     */
    private CollectionFactory $collectionFactory;
    /**
     * @var LocatorInterface
     */
    private LocatorInterface $locator;
    /**
     * @var UrlInterface
     */
    private UrlInterface $urlBuilder;
    private ArrayManager $arrayManager;


    /**
     * ExtraInformationCustomOptions constructor.
     * @param LocatorInterface $locator
     * @param SerializerInterface $serialize
     * @param CollectionFactory $collectionFactory
     * @param UrlInterface $urlBuilder
     */
    public function __construct(
        LocatorInterface    $locator,
        SerializerInterface $serialize,
        CollectionFactory   $collectionFactory,
        UrlInterface        $urlBuilder,
        ArrayManager        $arrayManager
    ) {
        $this->urlBuilder = $urlBuilder;
        $this->locator = $locator;
        $this->serialize = $serialize;
        $this->collectionFactory = $collectionFactory;
        $this->arrayManager = $arrayManager;
    }

    /**
     * @param array $data
     * @return array
     */
    public function modifyData(array $data)
    {
        $product = $this->locator->getProduct();
        $productId = $product->getId();
        $extraInformationCollection = $this->collectionFactory->create()->addFieldToFilter('product_id', $productId);
        $options = [];

        foreach ($extraInformationCollection as $extraInformationModel) {
            $options[] = [
                'option_id' => $extraInformationModel->getOptionId(),
                'extra_option_name' => $extraInformationModel->getName(),
                'values' => $this->convertDynamicValue($extraInformationModel),
            ];
        }

        $data[$productId][self::DATA_SOURCE_DEFAULT][self::GRID_VALUES_OPTIONS_NAME] = $options;

        return $data;
    }

    /**
     * @param $extraInformationModel
     * @return array|bool|float|int|string|null
     */
    public function convertDynamicValue($extraInformationModel)
    {
        $dynamicValue = $extraInformationModel->getDynamicValue();

        if ($dynamicValue != null) {
            $dynamicValue = $this->serialize->unserialize($dynamicValue);
        }

        return $dynamicValue;
    }

    /**
     * @param array $meta
     * @return array
     */
    public function modifyMeta(array $meta)
    {
        $this->meta = $meta;
        $this->ExtraInformationCustomOptions();

        return $this->meta;
    }

    /**
     *
     */
    protected function ExtraInformationCustomOptions()
    {
        $this->meta = array_replace_recursive(
            $this->meta,
            [
                static::GROUP_VALUES_OPTIONS_NAME => $this->getTabConfig(),
                self::IMPORT_OPTIONS_MODAL => $this->getImportOptionsModalConfig()
            ]
        );
    }

    /**
     * @return array
     */
    protected function getTabConfig()
    {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'label' => __('Extra Information Custom Options'),
                        'componentType' => Fieldset::NAME,
                        'dataScope' => \Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\CustomOptions::GROUP_CUSTOM_OPTIONS_SCOPE,
                        'collapsible' => true,
                        'sortOrder' => 42,
                    ],
                ],
            ],
            'children' => [
                static::CONTAINER_HEADER_NAME => $this->getHeaderContainerConfig(20),
                static::GRID_VALUES_OPTIONS_NAME => $this->getOptionsGridConfig(30),
            ]
        ];
    }

    /**
     * @param $sortOrder
     * @return array
     */
    protected function getHeaderContainerConfig($sortOrder)
    {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'label' => null,
                        'formElement' => Container::NAME,
                        'componentType' => Container::NAME,
                        'template' => 'ui/form/components/complex',
                        'sortOrder' => $sortOrder
                    ],
                ],
            ],
            'children' => [
                CustomOptionsModifier::BUTTON_IMPORT => $this->getDataImportButton(),
                static::BUTTON_ADD => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'title' => __('Add Information Fields'),
                                'formElement' => Container::NAME,
                                'componentType' => Container::NAME,
                                'component' => 'Magento_Ui/js/form/components/button',
                                'sortOrder' => 20,
                                'actions' => [
                                    [
                                        'targetName' => '${ $.ns }.${ $.ns }.' . static::GROUP_VALUES_OPTIONS_NAME
                                            . '.' . static::GRID_VALUES_OPTIONS_NAME,
                                        '__disableTmpl' => ['targetName' => false],
                                        'actionName' => 'processingAddChild',
                                    ]
                                ]
                            ]
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return \array[][][]
     */
    private function getDataImportButton()
    {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'title' => __('Import Extra Information'),
                        'formElement' => Container::NAME,
                        'componentType' => Container::NAME,
                        'component' => 'Magento_Ui/js/form/components/button',
                        'actions' => [
                            [
                                'targetName' => '${ $.ns }.${ $.ns }.' . static::GROUP_VALUES_OPTIONS_NAME
                                    . '.' . static::GRID_VALUES_OPTIONS_NAME,
                                'actionName' => 'processingAddChild',
                            ],
                            [
                                'targetName' => 'ns=' . self::FORM_NAME . ', index='
                                    . self::IMPORT_OPTIONS_MODAL,
                                'actionName' => 'openModal',
                            ],
                            [
                                'targetName' => 'ns=' . self::CUSTOM_OPTIONS_LISTING
                                    . ', index=' . self::CUSTOM_OPTIONS_LISTING,
                                'actionName' => 'render',
                            ],
                        ],
                        'displayAsLink' => true,
                        'sortOrder' => 10,
                    ],
                ],
            ],
        ];
    }

    /**
     * @param $sortOrder
     * @return array
     */
    protected function getOptionsGridConfig($sortOrder)
    {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'addButtonLabel' => __('Add Option'),
                        'componentType' => DynamicRows::NAME,
                        'component' => 'KodKodKod_CustomProductOption/js/components/dynamic-rows-import-custom-options',
                        'template' => 'ui/dynamic-rows/templates/collapsible',
                        'additionalClasses' => 'admin__field-wide',
                        'deleteProperty' => static::FIELD_IS_DELETE,
                        'deleteValue' => '1',
                        'addButton' => false,
                        'renderDefaultRecord' => false,
                        'columnsHeader' => false,
                        'collapsibleHeader' => true,
                        'sortOrder' => $sortOrder,
                        'dataProvider' => static::CUSTOM_OPTIONS_LISTING,
                        'imports' => [
                            'insertData' => '${ $.provider }:${ $.dataProvider }',
                            '__disableTmpl' => ['insertData' => false],
                        ],
                    ],
                ]
            ],
            'children' => [
                'record' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'headerLabel' => __('Extra Information'),
                                'componentType' => Container::NAME,
                                'component' => 'Magento_Ui/js/dynamic-rows/record',
                                'isTemplate' => true,
                                'is_collection' => true,
                            ]
                        ]
                    ],
                    'children' => [
                        static::CONTAINER_OPTION => [
                            'arguments' => [
                                'data' => [
                                    'config' => [
                                        'componentType' => Fieldset::NAME,
                                        'label' => null,
                                        'sortOrder' => 10,
                                        'opened' => false,
                                        'collapsible' => true
                                    ],
                                ],
                            ],
                            'children' => [
                                static::CONTAINER_COMMON_NAME => $this->getCommonContainerConfig(20),
                                static::GRID_TYPE_SELECT_NAME => $this->getSelectTypeGridConfig(30),
                            ]
                        ],
                    ]

                ]
            ]
        ];
    }

    /**
     * @param $sortOrder
     * @return array
     */
    protected function getCommonContainerConfig($sortOrder)
    {
        $commonContainer = [
            'arguments' => [
                'data' => [
                    'config' => [
                        'componentType' => Container::NAME,
                        'formElement' => Container::NAME,
                        'component' => 'Magento_Ui/js/form/components/group',
                        'breakLine' => false,
                        'showLabel' => false,
                        'additionalClasses' => 'admin__field-group-columns admin__control-group-equal',
                        'sortOrder' => $sortOrder,
                    ],
                ],
            ],
            'children' => [
                static::FIELD_OPTION_ID => $this->getOptionIdFieldConfig(10),
                'extra_option_name' => $this->getInputFieldConfig(
                    15,
                    __('Name'),
                    'extra_option_name',
                    Text::NAME,
                    true
                ),

            ]
        ];
        return $commonContainer;
    }

    /**
     * Get config for hidden id field
     *
     * @param int $sortOrder
     * @return array
     * @since 101.0.0
     */
    protected function getOptionIdFieldConfig($sortOrder)
    {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'formElement' => Input::NAME,
                        'componentType' => Field::NAME,
                        'dataScope' => static::FIELD_OPTION_ID,
                        'sortOrder' => $sortOrder,
                        'visible' => false,
                    ],
                ],
            ],
        ];
    }

    /**
     * @param $sortOrder
     * @param $label
     * @param $scope
     * @param $type
     * @param $isRequired
     * @return array
     */
    protected function getInputFieldConfig($sortOrder, $label, $scope, $type, $isRequired)
    {
        $validateNumber = true;

        $options = [
            'arguments' => [
                'data' => [
                    'config' => [
                        'label' => $label,
                        'component' => 'Magento_Catalog/component/static-type-input',
                        'valueUpdate' => 'input'
                    ],
                ],
            ],
        ];

        if ($type == Text::NAME) {
            $validateNumber = false;
        }

        return array_replace_recursive(
            [
                'arguments' => [
                    'data' => [
                        'config' => [
                            'label' => $label,
                            'componentType' => Field::NAME,
                            'formElement' => Input::NAME,
                            'dataScope' => $scope,
                            'dataType' => $type,
                            'sortOrder' => $sortOrder,
                            'validation' => [
                                'required-entry' => $isRequired,
                                'validate-greater-than-zero' => $validateNumber,
                            ],
                        ],
                    ],
                ],
            ],
            $options
        );
    }

    /**
     * @param $sortOrder
     * @return array
     */
    protected function getSelectTypeGridConfig($sortOrder)
    {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'addButtonLabel' => __('Add'),
                        'componentType' => DynamicRows::NAME,
                        'component' => 'Magento_Ui/js/dynamic-rows/dynamic-rows',
                        'additionalClasses' => 'admin__field-wide',
                        'deleteProperty' => static::FIELD_IS_DELETE,
                        'deleteValue' => '1',
                        'renderDefaultRecord' => false,
                        'sortOrder' => $sortOrder,
                    ],
                ],
            ],
            'children' => [
                'record' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'componentType' => Container::NAME,
                                'component' => 'Magento_Ui/js/dynamic-rows/record',
                                'isTemplate' => true,
                                'is_collection' => true,
                            ],
                        ],
                    ],
                    'children' => [
                        'extra_option_code' => [
                            'arguments' => [
                                'data' => [
                                    'config' => [
                                        'label' => __('Code'),
                                        'componentType' => Field::NAME,
                                        'formElement' => Input::NAME,
                                        'dataScope' => 'extra_option_code',
                                        'dataType' => Text::NAME,
                                        'sortOrder' => 10,
                                        'validation' => [
                                            'required-entry' => true
                                        ],
                                    ],
                                ],
                            ],
                        ],
                        'extra_option_title' => [
                            'arguments' => [
                                'data' => [
                                    'config' => [
                                        'label' => __('Title'),
                                        'componentType' => Field::NAME,
                                        'formElement' => Input::NAME,
                                        'dataScope' => 'extra_option_title',
                                        'dataType' => Text::NAME,
                                        'sortOrder' => 20,
                                        'validation' => [
                                            'required-entry' => true
                                        ],
                                    ],
                                ],
                            ],
                        ],
                        'extra_option_value' => [
                            'arguments' => [
                                'data' => [
                                    'config' => [
                                        'label' => __('Value'),
                                        'componentType' => Field::NAME,
                                        'formElement' => Textarea::NAME,
                                        'dataScope' => 'extra_option_value',
                                        'dataType' => Text::NAME,
                                        'sortOrder' => 30,
                                        'validation' => [
                                            'required-entry' => true
                                        ],
                                    ],
                                ],
                            ],
                        ],
                        'extra_option_dynamic_image' => $this->getImageFieldOptionConfig(800),
                        static::FIELD_IS_DELETE => $this->getIsDeleteFieldConfig(900)
                    ]
                ]
            ]
        ];
    }

    /**
     * @param $sortOrder
     * @return array
     */
    protected function getImageFieldOptionConfig($sortOrder)
    {
        $imageContainer['arguments']['data']['config'] = [
            'componentType' => Container::NAME,
            'formElement' => Container::NAME,
            'component' => 'Magento_Ui/js/form/components/group',
            'label' => __('Image'),
            'showLabel' => false,
            'dataScope' => '',
            'sortOrder' => $sortOrder,
        ];

        $imageUploader['arguments']['data']['config'] = [
            'formElement' => 'fileUploader',
            'componentType' => 'fileUploader',
            'component' => 'Magento_Ui/js/form/element/image-uploader',
            'elementTmpl' => 'KodKodKod_CustomProductOption/file-uploader',
            'previewTmpl' => 'Magento_Catalog/image-preview',
            'fileInputName' => 'extra_option_dynamic_image',
            'uploaderConfig' => [
                'url' => $this->urlBuilder->getUrl(
                    'wgt_custom_option/upload/image',
                    ['type' => 'extra_option_dynamic_image', '_secure' => true]
                ),
            ],
            'dataScope' => 'extra_option_dynamic_image'
        ];

        return $this->arrayManager->set(
            'children',
            $imageContainer,
            [
                'extra_option_dynamic_image' => $imageUploader
            ]
        );
    }

    /**
     * @param $sortOrder
     * @return \array[][][]
     */
    protected function getIsDeleteFieldConfig($sortOrder)
    {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'componentType' => ActionDelete::NAME,
                        'fit' => true,
                        'sortOrder' => $sortOrder
                    ],
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    private function getImportOptionsModalConfig()
    {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'componentType' => Modal::NAME,
                        'dataScope' => '',
                        'provider' => static::FORM_NAME . '.product_form_data_source',
                        'options' => [
                            'title' => __('Select Product'),
                            'buttons' => [
                                [
                                    'text' => __('Import'),
                                    'class' => 'action-primary',
                                    'actions' => [
                                        [
                                            'targetName' => 'index = ' . static::CUSTOM_OPTIONS_LISTING,
                                            'actionName' => 'save'
                                        ],
                                        'closeModal'
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
            'children' => [
                static::CUSTOM_OPTIONS_LISTING => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'autoRender' => false,
                                'componentType' => 'insertListing',
                                'dataScope' => static::CUSTOM_OPTIONS_LISTING,
                                'externalProvider' => static::CUSTOM_OPTIONS_LISTING . '.'
                                    . static::CUSTOM_OPTIONS_LISTING . '_data_source',
                                'selectionsProvider' => static::CUSTOM_OPTIONS_LISTING . '.'
                                    . static::CUSTOM_OPTIONS_LISTING . '.product_columns.ids',
                                'ns' => static::CUSTOM_OPTIONS_LISTING,
                                'render_url' => $this->urlBuilder->getUrl('mui/index/render'),
                                'realTimeLink' => true,
                                'externalFilterMode' => false,
                                'currentProductId' => $this->locator->getProduct()->getId(),
                                'dataLinks' => [
                                    'imports' => false,
                                    'exports' => true
                                ],
                                'exports' => [
                                    'currentProductId' => '${ $.externalProvider }:params.current_product_id',
                                    '__disableTmpl' => ['currentProductId' => false],
                                ]
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }
}
