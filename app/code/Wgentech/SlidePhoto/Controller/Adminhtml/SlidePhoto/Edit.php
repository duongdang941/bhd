<?php

namespace Wgentech\SlidePhoto\Controller\Adminhtml\SlidePhoto;

use Wgentech\SlidePhoto\Controller\Adminhtml\SlidePhoto;
use Wgentech\SlidePhoto\Model\SlidePhotoFactory;

class Edit extends SlidePhoto
{
    const ADMIN_RESOURCE = 'Wgentech_SlidePhoto::edit';

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var SlidePhotoFactory
     */
    protected $slidePhotoFactory;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        SlidePhotoFactory $slidePhotoFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->slidePhotoFactory = $slidePhotoFactory;
        parent::__construct($context, $coreRegistry);
    }

    public function execute()
    {
        // 1. Get ID and create model
        $id = $this->getRequest()->getParam('id');
        $model = $this->slidePhotoFactory->create();

        // 2. Initial checking
        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->messageManager->addErrorMessage(__('This Slide Photo no longer exists.'));

                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }
        }
        $this->_coreRegistry->register('wgentech_slide_photo', $model);

        // 3. Build edit form
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $this->initPage($resultPage)->addBreadcrumb(
            $id ? __('Edit Slide Photo') : __('New Slide Photo'),
            $id ? __('Edit Slide Photo') : __('New Slide Photo')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Slide Photo'));
        $resultPage->getConfig()->getTitle()->prepend($model->getId() ? __('Edit Slide Photo %1', $model->getId()) : __('New Slide Photo'));
        return $resultPage;
    }

}