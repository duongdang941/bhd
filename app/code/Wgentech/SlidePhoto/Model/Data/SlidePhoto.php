<?php

namespace Wgentech\SlidePhoto\Model\Data;

use Magento\Framework\DataObject;
use Wgentech\SlidePhoto\Api\Data\SlidePhotoInterface;

class SlidePhoto extends DataObject implements SlidePhotoInterface
{
    /**
     * @inheritDoc
     */
    public function getId(): ?int
    {
        return $this->getData(self::ID) === null ? null
            : (int)$this->getData(self::ID);
    }

    /**
     * @inheritDoc
     */
    public function setId(?int $id): void
    {
        $this->setData(self::ID, $id);
    }

    /**
     * @inheritDoc
     */
    public function getImage(): ?string
    {
        return $this->getData(self::IMAGE);
    }

    /**
     * @inheritDoc
     */
    public function setImage(?string $image): void
    {
        $this->setData(self::IMAGE, $image);
    }

    /**
     * @inheritDoc
     */
    public function getTitle(): ?string
    {
        return $this->getData(self::TITLE);
    }

    /**
     * @inheritDoc
     */
    public function setTitle(?string $title): void
    {
        $this->setData(self::TITLE, $title);
    }

    /**
     * @inheritDoc
     */
    public function getText(): ?string
    {
        return $this->getData(self::TEXT);
    }

    /**
     * @inheritDoc
     */
    public function setText(?string $text): void
    {
        $this->setData(self::TEXT, $text);
    }

    /**
     * @inheritDoc
     */
    public function getUrl(): ?string
    {
        return $this->getData(self::URL);
    }

    /**
     * @inheritDoc
     */
    public function setUrl(?string $url): void
    {
        $this->setData(self::URL, $url);
    }

    /**
     * @inheritDoc
     */
    public function getIsActive(): ?bool
    {
        return $this->getData(self::IS_ACTIVE) === null ? null
            : (bool)$this->getData(self::IS_ACTIVE);
    }

    /**
     * @inheritDoc
     */
    public function setIsActive(?bool $isActive): void
    {
        $this->setData(self::IS_ACTIVE, $isActive);
    }

    /**
     * @inheritDoc
     */
    public function getButtonTitle(): ?string
    {
        return $this->getData(self::BUTTON_TITLE);
    }

    /**
     * @inheritDoc
     */
    public function setButtonTitle(?string $buttonTitle): void
    {
        $this->setData(self::BUTTON_TITLE, $buttonTitle);
    }
}
