<?php

namespace Wgentech\CustomContact\Model;

use Magento\Contact\Model\ConfigInterface;
use Magento\Framework\App\Area;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Framework\Translate\Inline\StateInterface;
use Magento\Store\Model\StoreManagerInterface;


class MailContactCustomer extends \Magento\Contact\Model\Mail
{
    /**
     * @var ConfigInterface
     */
    private $contactsConfig;
    /**
     * @var TransportBuilder
     */
    private $transportBuilder;
    /**
     * @var StateInterface
     */
    private $inlineTranslation;
    /**
     * @var StoreManagerInterface|mixed
     */
    private $storeManager;

    /**
     * @param ConfigInterface $contactsConfig
     * @param TransportBuilder $transportBuilder
     * @param StateInterface $inlineTranslation
     * @param StoreManagerInterface|null $storeManager
     */
    public function __construct(
        ConfigInterface       $contactsConfig,
        TransportBuilder      $transportBuilder,
        StateInterface        $inlineTranslation,
        StoreManagerInterface $storeManager = null
    )
    {
        $this->contactsConfig = $contactsConfig;
        $this->transportBuilder = $transportBuilder;
        $this->inlineTranslation = $inlineTranslation;
        $this->storeManager = $storeManager ?: ObjectManager::getInstance()->get(StoreManagerInterface::class);
        parent::__construct($contactsConfig, $transportBuilder, $inlineTranslation, $storeManager);
    }

    public function sendEmailCustomer($variables, $emailTemplate)
    {

        $sendTo = $variables['email'] ?? '';
        $this->inlineTranslation->suspend();
        try {
            $transport = $this->transportBuilder
                ->setTemplateIdentifier($emailTemplate)
                ->setTemplateOptions(
                    [
                        'area' => Area::AREA_FRONTEND,
                        'store' => $this->storeManager->getStore()->getId()
                    ]
                )
                ->setTemplateVars([])
                ->setFrom($this->contactsConfig->emailSender())
                ->addTo($sendTo)
                ->getTransport();

            $transport->sendMessage();
        } finally {
            $this->inlineTranslation->resume();
        }
    }
}
