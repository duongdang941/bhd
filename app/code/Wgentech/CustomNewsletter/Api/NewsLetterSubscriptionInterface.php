<?php

namespace Wgentech\CustomNewsletter\Api;

interface NewsLetterSubscriptionInterface
{
    /**
     * @param string $customerEmail
     * @return mixed
     */
    public function postNewsLetter($customerEmail);
}
