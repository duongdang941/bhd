<?php

namespace KodKodKod\CustomProductOption\Helper;

use Magento\Catalog\Model\Product\Option;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Exception\NoSuchEntityException;
use KodKodKod\CustomProductOption\Model\ProductCustomOptionValues;
use KodKodKod\CustomProductOption\Model\ProductCustomOptionValuesFactory;

class OptionsProvider extends AbstractHelper
{
    /**
     * @var ProductCustomOptionValuesFactory
     */
    private $productCustomOptionValues;
    /**
     * @var Data
     */
    private Data $helperData;

    /**
     * OptionsProvider constructor.
     *
     * @param Context $context
     * @param ProductCustomOptionValuesFactory $productCustomOptionValues
     * @param Data $helperData
     */
    public function __construct(
        Context $context,
        ProductCustomOptionValuesFactory $productCustomOptionValues,
        Data $helperData
    ) {
        $this->helperData = $helperData;
        $this->productCustomOptionValues = $productCustomOptionValues;
        parent::__construct($context);
    }

    /**
     * @param $options
     * @return mixed
     * @throws NoSuchEntityException
     */
    public function getExtendedOptionsConfig($options)
    {
        foreach ($options as $key => $option) {
            /** @var Option $option */
            $extensionAttributes = $option->getExtensionAttributes();
            $wgtCommonImage = null;
            $wgtCommonImageName = $option->getWgtCommonImage();

            if ($wgtCommonImageName) {
                $wgtCommonImage = $this->helperData->getWgtMediaUrl($wgtCommonImageName);
            }

            $extensionAttributes->setWgtCommonImage($wgtCommonImage);
            $extensionAttributes->setWgtCommonQuestionCode($option->getWgtCommonQuestionCode());
            $extensionAttributes->setWgtCommonDependOn($option->getWgtCommonDependOn());
            $extensionAttributes->setWgtCommonExtraInformation($option->getWgtCommonExtraInformation());
            $extensionAttributes->setWgtTemplateId($option->getWgtTemplateId());
            $extensionAttributes->setWgtFormulatedOption($option->getWgtFormulatedOption());
            $extensionAttributes->setWgtAnswerOption($option->getWgtAnswerOption());
            $extensionAttributes->setWgtOptionRequired($option->getWgtOptionRequired());

            $values = $option->getValues();
            $values = $this->addCustomFieldForValues($values);

            if ($values) {
                $extensionAttributes->setValues($values);
            }

            $option->setExtensionAttributes($extensionAttributes);
            $options[$key] = $option;
        }
        return $options;
    }

    /**
     * @param $values
     * @return array
     * @throws NoSuchEntityException
     */
    private function addCustomFieldForValues($values)
    {
        if (!is_array($values)) {
            return $values;
        }
        $optionValueModelArray = [];

        foreach ($values as $key => $value) {
            /** @var ProductCustomOptionValues $optionValueModel */
            $optionValueModel = $this->productCustomOptionValues->create();
            $wgtValueDynamicImageName = $value->getWgtValueDynamicImage();
            $wgtValueDynamicImage = null;

            if ($wgtValueDynamicImageName) {
                $wgtValueDynamicImage = $this->helperData->getWgtMediaUrl($wgtValueDynamicImageName);
            }

            $optionCode = $value->getOptionCode();
            $dependOn = $value->getDependOn();
            $optionTypeId = $value->getOptionTypeId();
            $wgtExtraInformation = $value->getWgtExtraInformation();

            $optionValueModel->setWgtValueDynamicImage($wgtValueDynamicImage);
            $optionValueModel->setOptionCode($optionCode);
            $optionValueModel->setDependOn($dependOn);
            $optionValueModel->setOptionTypeId($optionTypeId);
            $optionValueModel->setWgtExtraInformation($wgtExtraInformation);

            $optionValueModelArray[$key] = $optionValueModel;
        }

        return $optionValueModelArray;
    }


}
