<?php

namespace KodKodKod\CustomProductOption\Plugin\Magento\Tax\Model\Sales\Total\Quote;

use Magento\Quote\Model\Quote\Item\AbstractItem;
use Magento\Tax\Api\Data\QuoteDetailsItemInterfaceFactory;
use Magento\Tax\Model\Sales\Total\Quote\CommonTaxCollector;

class CommonTaxCollectorPlugin
{
    /**
     * @param CommonTaxCollector $subject
     * @param $result
     * @param QuoteDetailsItemInterfaceFactory $itemDataObjectFactory
     * @param AbstractItem $item
     * @param $priceIncludesTax
     * @param $useBaseCurrency
     * @param $parentCode
     */
    public function afterMapItem(
        CommonTaxCollector               $subject,
                                         $result,
        QuoteDetailsItemInterfaceFactory $itemDataObjectFactory,
        AbstractItem                     $item,
                                         $priceIncludesTax,
                                         $useBaseCurrency,
                                         $parentCode = null
    )
    {
        $option = $item->getOptionByCode('wgt_custom_option_price');
        $product = $item->getProduct();
        if ($option && $product) {
            // split price - item price without custom options, and custom options as a separate taxable item
            // handle case price product = 0
            if ($result->getUnitPrice() - $option->getValue() <= 0) {
                $priceIncludesTaxOption = $option->getValue();
            } else {
                $priceIncludesTaxOption = $result->getUnitPrice();
            }
            $result->setUnitPrice($priceIncludesTaxOption);
            $result->setUnitPrice($result->getUnitPrice());
            $extraTaxables = $item->getAssociatedTaxables() ?: [];
            $extraTaxables[] = [
                'price_includes_tax' => false,
                $subject::KEY_ASSOCIATED_TAXABLE_TYPE => 'wgt-custom-options',
                $subject::KEY_ASSOCIATED_TAXABLE_CODE => 'wgt-custom-options-' . $item->getTaxCalculationItemId(),
                $subject::KEY_ASSOCIATED_TAXABLE_BASE_UNIT_PRICE => 0,
                $subject::KEY_ASSOCIATED_TAXABLE_UNIT_PRICE => 0,
                $subject::KEY_ASSOCIATED_TAXABLE_QUANTITY => $item->getTotalQty(),
                $subject::KEY_ASSOCIATED_TAXABLE_TAX_CLASS_ID => $product->getTaxClassId(),
            ];
            $item->setAssociatedTaxables($extraTaxables);
        }

        return $result;
    }
}
