<?php

namespace Wgentech\CustomContact\Plugin\Magento\Framework\View\Element\UiComponent\DataProvider;

use Magento\Framework\Api\Filter;
use Magento\Framework\Data\Collection;
use Magento\Framework\View\Element\UiComponent\DataProvider\RegularFilter;
use Wgentech\CustomContact\Plugin\Magento\Framework\View\Element\UiComponent\DataProvider\Reporting\ReportingPlugin;

class RegularFilterPlugin
{
    /**
     * @param RegularFilter $subject
     * @param callable $proceed
     * @param Collection $collection
     * @param Filter $filter
     * @return void
     */
    public function aroundApply(
        RegularFilter $subject,
        callable      $proceed,
        Collection    $collection,
        Filter        $filter
    )
    {
        if ($collection instanceof \Magento\Customer\Model\ResourceModel\Grid\Collection) {
            if ($filter->getField() != ReportingPlugin::HAS_ORDER) {
                $proceed($collection, $filter);
            }
        }

    }

}
