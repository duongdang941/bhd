<?php

namespace KodKodKod\CustomApi\Plugin\Magento\Bundle\Model\Product;

use Magento\Bundle\Model\Product\LinksList;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;

class LinksListPlugin
{
    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @param ProductRepositoryInterface $productRepository
     */
    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepository =  $productRepository;
    }

    /**
     * @param \Magento\Bundle\Model\Product\LinksList $subject
     * @param $result
     * @param ProductInterface $product
     * @param int $optionId
     */
    public function afterGetItems(\Magento\Bundle\Model\Product\LinksList $subject, $result, ProductInterface $product, $optionId)
    {
        $productLinks = [];

        foreach ($result as $productLink) {
            $sku = $productLink->getSku();
            $product = $this->productRepository->get($sku);
            $formeVoile = $product->getResource()->getAttribute('forme_voile')->getFrontend()->getValue($product);
            $couleurVoile = $product->getResource()->getAttribute('couleur_voile')->getFrontend()->getValue($product);
            $extensionAttributes = $productLink->getExtensionAttributes();
            $extensionAttributes->setFormeVoile($formeVoile);
            $extensionAttributes->setCouleurVoile($couleurVoile);
            $productLink->setExtensionAttributes($extensionAttributes);
            $productLinks[] = $productLink;
        }

        return $productLinks;
    }
}
