<?php

declare(strict_types = 1);

namespace KodKodKod\CustomVsbridgeIndexerCatalog\Helper;

use KodKodKod\CustomProductOption\Helper\Data;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\DB\Select;
use Magento\Framework\App\ResourceConnection;
use Magento\Store\Model\Store;


class HelperOption  extends AbstractHelper
{
    /**
     * @var ResourceConnection
     */
    private ResourceConnection $resource;
    /**
     * @var Data
     */
    private Data $helperData;

    /**
     * @param Context $context
     * @param ResourceConnection $resourceModel
     * @param Data $helperData
     */
    public function __construct(
        Context $context,
        ResourceConnection $resourceModel,
        Data $helperData
    )
    {
        $this->resource = $resourceModel;
        $this->helperData = $helperData;
        parent::__construct($context);
    }
    /**
     * @param array $linkFieldIds
     * @param int $storeId
     *
     * @return array
     */
    public function loadProductOptions(array $linkFieldIds, int $storeId): array
    {
        $select = $this->getProductOptionSelect($linkFieldIds, $storeId);

        $result = $this->getConnection()->fetchAssoc($select);
        return $this->convertOption($result);
    }

    private function convertOption($result) {

        foreach ($result as $productOptionsId  => $productOptionValue) {
            $wgtCommonExtraInformation = $productOptionValue['wgt_common_extra_information'] ?? null;
            $wgtCommonImage = $productOptionValue['wgt_common_image'] ?? null;
            if ($wgtCommonExtraInformation) {
                $result[$productOptionsId]['wgt_common_extra_information'] = $this->helperData->getExtraInformationCustomOptions($wgtCommonExtraInformation);
            }
            if ($wgtCommonImage) {
                $result[$productOptionsId]['wgt_common_image'] = $this->helperData->getWgtMediaUrl($wgtCommonImage);
            }
        }

        return $result;
    }

    /**
     * @param array $linkFieldIds
     * @param int $storeId
     *
     * @return Select
     */
    private function getProductOptionSelect(array $linkFieldIds, int $storeId): Select
    {
        $connection = $this->getConnection();
        $mainTableAlias = 'main_table';

        $select = $connection->select()->from(
            [$mainTableAlias => $this->resource->getTableName('catalog_product_option')]
        );

        $select->where($mainTableAlias.  '.product_id IN (?)', $linkFieldIds)->order('sort_order ASC');

        $select = $this->addTitleToResult($select, $storeId);
        return $this->addPriceToResult($select, $storeId);
    }

    /**
     * @param Select $select
     * @param int $storeId
     *
     * @return Select
     */
    private function addTitleToResult(Select $select, int $storeId): Select
    {
        $productOptionTitleTable = $this->resource->getTableName('catalog_product_option_title');
        $connection = $this->getConnection();
        $titleExpr = $connection->getCheckSql(
            'store_option_title.title IS NULL',
            'default_option_title.title',
            'store_option_title.title'
        );

        $select->join(
            ['default_option_title' => $productOptionTitleTable],
            'default_option_title.option_id = main_table.option_id',
            ['default_title' => 'title']
        )->joinLeft(
            ['store_option_title' => $productOptionTitleTable],
            'store_option_title.option_id = main_table.option_id AND ' . $connection->quoteInto(
                'store_option_title.store_id = ?',
                $storeId
            ),
            [
                'store_title' => 'title',
                'title' => $titleExpr
            ]
        )->where(
            'default_option_title.store_id = ?',
            Store::DEFAULT_STORE_ID
        );

        return $select;
    }

    /**
     * @param Select $select
     * @param int $storeId
     *
     * @return Select
     */
    private function addPriceToResult(Select $select, int $storeId): Select
    {
        $productOptionPriceTable = $this->resource->getTableName('catalog_product_option_price');
        $connection = $this->getConnection();
        $priceExpr = $connection->getCheckSql(
            'store_option_price.price IS NULL',
            'default_option_price.price',
            'store_option_price.price'
        );
        $priceTypeExpr = $connection->getCheckSql(
            'store_option_price.price_type IS NULL',
            'default_option_price.price_type',
            'store_option_price.price_type'
        );

        $select->joinLeft(
            ['default_option_price' => $productOptionPriceTable],
            'default_option_price.option_id = main_table.option_id AND ' . $connection->quoteInto(
                'default_option_price.store_id = ?',
                Store::DEFAULT_STORE_ID
            ),
            [
                'default_price' => 'price',
                'default_price_type' => 'price_type'
            ]
        )->joinLeft(
            ['store_option_price' => $productOptionPriceTable],
            'store_option_price.option_id = main_table.option_id AND ' . $connection->quoteInto(
                'store_option_price.store_id = ?',
                $storeId
            ),
            [
                'store_price' => 'price',
                'store_price_type' => 'price_type',
                'price' => $priceExpr,
                'price_type' => $priceTypeExpr
            ]
        );

        return $select;
    }

    /**
     * @return \Magento\Framework\DB\Adapter\AdapterInterface
     */
    private function getConnection()
    {
        return $this->resource->getConnection();
    }
}
