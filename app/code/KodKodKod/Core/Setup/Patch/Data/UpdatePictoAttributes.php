<?php
declare(strict_types=1);

namespace KodKodKod\Core\Setup\Patch\Data;

use Magento\Eav\Model\ResourceModel\Entity\Attribute;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Eav\Model\Entity\Attribute\Source\Boolean;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;

class UpdatePictoAttributes implements DataPatchInterface
{
    const ATTRIBUTES_UPDATE =
        [
            'custom_product_attribute' => 'Produit sur mesure',
            'eco_responsible_materials' => 'Matière Ecoresponsables',
            'without_intermediary' => 'Sans intermédiaire. Direct usine.'
        ];
    /**
     * @var EavSetupFactory
     */
    private EavSetupFactory $eavSetupFactory;
    /**
     * @var Attribute
     */
    private Attribute $eavAttribute;
    /**
     * @var ModuleDataSetupInterface
     */
    private ModuleDataSetupInterface $moduleDataSetup;

    /**
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param EavSetupFactory $eavSetupFactory
     * @param Attribute $eavAttribute
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        EavSetupFactory          $eavSetupFactory,
        Attribute                $eavAttribute
    )
    {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->eavAttribute = $eavAttribute;
        $this->moduleDataSetup = $moduleDataSetup;
    }

    public function apply(): void
    {

        $this->moduleDataSetup->getConnection()->startSetup();
        /** @var EavSetupFactory $eavSetup */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $this->moduleDataSetup]);
        foreach (self::ATTRIBUTES_UPDATE as $attributeCode => $attributeLabel) {
            $this->addPictoAttributes($eavSetup, $attributeCode, $attributeLabel);
        }
        $this->moduleDataSetup->getConnection()->endSetup();
    }

    private function addPictoAttributes($eavSetup, $attributeCode, $attributeLabel)
    {
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            $attributeCode,
            [
                'group' => 'General',
                'type' => 'int',
                'backend' => '',
                'frontend' => '',
                'label' => $attributeLabel,
                'input' => 'boolean',
                'class' => '',
                'source' => Boolean::class,
                'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                'visible' => true,
                'required' => false,
                'user_defined' => false,
                'default' => '1',
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => false,
                'unique' => false,
                'apply_to' => 'simple,configurable,virtual,bundle,downloadable'
            ]
        );
    }

    /**
     * {@inheritdoc}
     *
     * @return string[]
     */
    public static function getDependencies(): array
    {
        return [];
    }

    /**
     * {@inheritdoc}
     *
     * @return string[]
     */
    public function getAliases(): array
    {
        return [];
    }
}
