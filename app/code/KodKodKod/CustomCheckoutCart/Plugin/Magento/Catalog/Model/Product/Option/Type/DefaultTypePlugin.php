<?php
/**
 *
 * @copyright Copyright © 2021 Wgentech. All rights reserved.
 * @author    linh.pv68@gmail.com
 */

namespace KodKodKod\CustomCheckoutCart\Plugin\Magento\Catalog\Model\Product\Option\Type;

use KodKodKod\CustomCheckoutCart\Services\Product;
use KodKodKod\CustomProductOption\Setup\Patch\Data\CreateCustomProductAttribute;
use Magento\Catalog\Model\Product\Option\Type\DefaultType;
use Magento\Framework\Exception\LocalizedException;
use Psr\Log\LoggerInterface;

/**
 * Class DefaultTypePlugin
 * KodKodKod\CustomCheckoutCart\Plugin\Magento\Catalog\Model\Product\Option\Type
 */
class DefaultTypePlugin
{
    const FORMULATED_FOR_CALCULATE_PRICE = 'self';
    const FORMULATED_WITH_SELF_VALUE = 'self.value';
    const OPTION_VALUE_EQUAL_ZERO = 0;
    /**
     * @var array
     */
    private array $paramsCalculate;

    /**
     * @var Product
     */
    private Product $productServices;

    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;

    /**
     * DefaultTypePlugin constructor.
     *
     * @param Product $productServices
     * @param LoggerInterface $logger
     */
    public function __construct(
        Product         $productServices,
        LoggerInterface $logger
    )
    {
        $this->logger = $logger;
        $this->productServices = $productServices;
    }


    /**
     * @param DefaultType $subject
     * @param $result
     * @param $optionValue
     * @param $basePrice
     * @return float|int|mixed
     */
    public function afterGetOptionPrice(DefaultType $subject, $result, $optionValue, $basePrice)
    {

        $priceOption = $optionValue == self::OPTION_VALUE_EQUAL_ZERO ? self::OPTION_VALUE_EQUAL_ZERO : $result;
        $product = $this->productServices->getProduct();

        if (empty($product)) {
            return $priceOption;
        }

        try {
            $wgtFormulated = $product->getData(CreateCustomProductAttribute::WGT_FORMULATED_ATTRIBUTE_CODE);
            $option = $subject->getOption();
            $wgtFormulatedOption = $option->getData(CreateCustomProductAttribute::WGT_FORMULATED_ATTRIBUTE_OPTION_CODE);

            if ($wgtFormulatedOption && str_contains($wgtFormulatedOption, self::FORMULATED_FOR_CALCULATE_PRICE)) {
                $priceOption = $this->getPriceOptionTypePercent($option, $optionValue, $priceOption);
                if (str_contains($wgtFormulatedOption, self::FORMULATED_WITH_SELF_VALUE)) {
                    $priceOption = $this->calculateNewPriceSelfValue($priceOption, $wgtFormulatedOption, $optionValue);
                } else {
                    $priceOption = $this->calculateNewPrice($priceOption, $wgtFormulatedOption, $wgtFormulated);
                }
            }
        } catch (LocalizedException $e) {
            $this->logger->error($e->getMessage());
        }

        return $priceOption;
    }

    /**
     * @param $currentPrice
     * @param $wgtFormulatedOption
     * @param $wgtFormulated
     * @return float|int|mixed
     */
    private function calculateNewPrice($currentPrice, $wgtFormulatedOption, $wgtFormulated)
    {
        $product = $this->productServices->getProduct();
        if (empty($product)) {
            return $currentPrice;
        }

        try {
            $optionIds = $product->getCustomOption('option_ids');
            $listOptionObject = $this->getListOption($optionIds, $product);
            $this->prepareParamForCalculate($listOptionObject, $product);
            $acreage = $wgtFormulated ? $this->calculateAcreage($wgtFormulated) : 0;
            $wgtFormulatedOption = $this->prepareFormulatedString($wgtFormulatedOption);

            $replaceString = [
                '{{surface}}' => $acreage,
                '{{self.price}}' => $currentPrice
            ];
            $wgtFormulatedOptionString = str_replace(' ', '', $wgtFormulatedOption);
            $wgtFormulatedOptionString = strtr($wgtFormulatedOptionString, $replaceString);
            $currentPrice = eval('return ' . $wgtFormulatedOptionString . ';');
        } catch (\Exception $exception) {
            $this->logger->error($exception->getMessage());
        }

        return $currentPrice;
    }

    private function calculateNewPriceSelfValue($currentPrice, $wgtFormulatedOption, $optionValue)
    {
        $product = $this->productServices->getProduct();
        if (empty($product)) {
            return $currentPrice;
        }

        try {
            $wgtFormulatedOption = $this->prepareFormulatedString($wgtFormulatedOption);
            $replaceString = [
                '{{self.price}}' => $currentPrice,
                '{{self.value}}' => (float)$optionValue
            ];
            $wgtFormulatedOptionString = str_replace(' ', '', $wgtFormulatedOption);
            $wgtFormulatedOptionString = strtr($wgtFormulatedOptionString, $replaceString);
            $currentPrice = eval('return ' . $wgtFormulatedOptionString . ';');
        } catch (\Exception $exception) {
            $this->logger->error($exception->getMessage());
        }

        return $currentPrice;
    }


    /**
     * @param $wgtFormulated
     * @return mixed
     */
    private function calculateAcreage($wgtFormulated)
    {
        $acreageString = $this->prepareFormulatedString($wgtFormulated);

        return eval('return ' . $acreageString . ';');
    }

    /**
     * @param $wgtFormulated
     * @return string
     */
    private function prepareFormulatedString($wgtFormulated)
    {
        $replaceString = [];

        foreach ($this->paramsCalculate as $key => $value) {
            $replaceString['{{' . $key . '}}'] = $value;
        }

        $wgtFormulated = str_replace(' ', '', $wgtFormulated);

        return strtr($wgtFormulated, $replaceString);
    }


    /**
     * @param $optionIds
     * @param $product
     * @return array
     */
    private function getListOption($optionIds, $product)
    {
        $listOptions = [];

        foreach (explode(',', $optionIds->getValue()) as $optionId) {
            if ($option = $product->getOptionById($optionId)) {
                $listOptions[] = $option;
            }
        }

        return $listOptions;
    }


    /**
     * @param $listOption
     * @param $product
     * @param string $type
     * @return array
     */
    private function prepareParamForCalculate($listOption, $product, $type = 'common')
    {
        $this->paramsCalculate = [];

        foreach ($listOption as $option) {
            $wgtCommonQuestionCode = $option->getData("wgt_common_question_code");
            if ($wgtCommonQuestionCode) {
                $optionId = $option->getId();
                $confItemOption = $product->getCustomOption('option_' . $optionId);
                // will return with cm value
                $optionValue = $confItemOption->getValue();

                if ($optionValue != null) {
                    // convert param to calculate cm to m
                    $this->paramsCalculate[$wgtCommonQuestionCode] = (float)$optionValue / 100;
                }
            }
        }

        return $this->paramsCalculate;
    }

    /**
     * @param $option
     * @param $optionValue
     * @param $priceOption
     * @return float|int|mixed
     */
    private function getPriceOptionTypePercent($option, $optionValue, $priceOption)
    {
        $result = $option->getValueById($optionValue) == null ? $option : $option->getValueById($optionValue);

        if ($result->getPriceType() == 'percent') {
            $priceOption = $result->getDefaultPrice() / 100;
        }

        return $priceOption;
    }
}
