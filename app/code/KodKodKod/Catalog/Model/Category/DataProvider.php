<?php

namespace KodKodKod\Catalog\Model\Category;

class DataProvider extends \Magento\Catalog\Model\Category\DataProvider
{
    protected function getFieldsMap()
    {
        $fields = parent::getFieldsMap();
        $fields['content'][] = 'image_category';
        $fields['content'][] = 'background_image';

        return $fields;
    }
}
