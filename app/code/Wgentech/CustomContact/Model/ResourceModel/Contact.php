<?php

namespace Wgentech\CustomContact\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Contact extends AbstractDb
{
    /**
     * @var string
     */
    protected $_eventPrefix = 'wgentech_contact_resource_model';

    /**
     * @inheritdoc
     */
    protected function _construct()
    {
        $this->_init('wgentech_contact', 'id');
        $this->_useIsObjectNew = true;
    }
}
