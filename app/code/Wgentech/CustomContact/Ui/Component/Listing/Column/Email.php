<?php

namespace Wgentech\CustomContact\Ui\Component\Listing\Column;
use Magento\Ui\Component\Listing\Columns\Column;

class Email extends Column
{
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as $key => $item) {
                if (isset($item['email'])) {
                    $mailTo = 'mailto: ' . $item['email'];
                    $dataSource['data']['items'][$key]['email'] = '<a href="' . $mailTo . '">' . $item['email'] . '</a>';
                } else {
                    $dataSource['data']['items'][$key]['email'] = '';
                }
            }
        }

        return $dataSource;
    }
}
