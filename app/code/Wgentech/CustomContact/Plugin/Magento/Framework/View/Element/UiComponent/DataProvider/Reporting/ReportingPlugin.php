<?php

namespace Wgentech\CustomContact\Plugin\Magento\Framework\View\Element\UiComponent\DataProvider\Reporting;

use Magento\Framework\Api\Search\SearchCriteriaInterface;
use Magento\Framework\Api\Search\SearchResultInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\View\Element\UiComponent\DataProvider\Reporting;

class ReportingPlugin
{
    const HAS_ORDER = 'has_order';

    /**
     * @param Reporting $subject
     * @param SearchResultInterface $result
     * @param SearchCriteriaInterface $searchCriteria
     * @return SearchResultInterface
     * @throws LocalizedException
     */
    public function afterSearch(
        Reporting               $subject,
        SearchResultInterface   $result,
        SearchCriteriaInterface $searchCriteria
    )
    {
        if ($searchCriteria->getRequestName() == 'customer_listing_data_source') {
            $filterGroups = $searchCriteria->getFilterGroups();
            foreach ($filterGroups as $group) {
                $filters = $group->getFilters();
                if (!empty($filters)) {
                    foreach ($filters as $item) {
                        if ($item->getField() == self::HAS_ORDER) {
                            if ($item->getValue() == true) {
                                $result->getSelect()
                                    ->joinLeft(
                                        ['so' => $result->getTable('sales_order')],
                                        'main_table.entity_id = so.customer_id',
                                        []
                                    )->distinct()->where('so.customer_id IS NOT NULL');
                                $result->getSelect()->group('main_table.entity_id');
                            } else {
                                $result->getSelect()
                                    ->joinLeft(
                                        ['so' => $result->getTable('sales_order')],
                                        'main_table.entity_id = so.customer_id',
                                        []
                                    )->distinct()->where('so.customer_id IS NULL');
                                $result->getSelect()->group('main_table.entity_id');
                            }
                        }
                    }
                }
            }
        }

        return $result;
    }
}
