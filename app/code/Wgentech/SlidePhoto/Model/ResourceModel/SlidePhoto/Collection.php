<?php

namespace Wgentech\SlidePhoto\Model\ResourceModel\SlidePhoto;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Wgentech\SlidePhoto\Model\ResourceModel\SlidePhoto as ResourceModel;
use Wgentech\SlidePhoto\Model\SlidePhoto as Model;

class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_eventPrefix = 'wgentech_slide_photo_collection';

    /**
     * @inheritdoc
     */
    protected function _construct()
    {
        $this->_init(Model::class, ResourceModel::class);
    }
}
