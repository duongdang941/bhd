<?php

namespace KodKodKod\OrderCustomStatus\Plugin\Magento\Sales\Controller\Order\Plugin;

use Magento\Framework\App\ActionInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Sales\Controller\Order\Plugin\Authentication;

class AuthenticationPlugin
{
    /**
     * @var RequestInterface
     */
    private $request;

    public function __construct(
        \Magento\Framework\App\RequestInterface $request
    )
    {
        $this->request = $request;
    }

    /**
     * @param Authentication $subject
     * @param callable $proceed
     * @param ActionInterface $action
     * @param RequestInterface $request
     * @return void
     */
    public function aroundBeforeDispatch(Authentication $subject, callable $proceed, ActionInterface $action, RequestInterface $request)
    {
        $params = $this->request->getParams();

        if (!empty($params) && !empty($params['from_vue']) && !empty($params['customer_id'])) {

            return;
        }

        return $proceed($action, $request);
    }
}
