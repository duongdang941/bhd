<?php

declare(strict_types=1);

namespace Shadhoc\ReviewApi\Model\Review\Validator;

use Shadhoc\ReviewApi\Api\Data\ReviewInterface;
use Shadhoc\ReviewApi\Validation\ValidationResult;
use Shadhoc\ReviewApi\Validation\ValidationResultFactory;
use Shadhoc\ReviewApi\Model\ReviewValidatorInterface;

/**
 * Class TitleValidator - validates review title
 */
class TitleValidator implements ReviewValidatorInterface
{
    /**
     * @var ValidationResultFactory
     */
    private $validationResultFactory;

    /**
     * @param ValidationResultFactory $validationResultFactory
     */
    public function __construct(ValidationResultFactory $validationResultFactory)
    {
        $this->validationResultFactory = $validationResultFactory;
    }

    /**
     * Check if review title is not empty
     *
     * @param ReviewInterface $review
     *
     * @return ValidationResult
     */
    public function validate(ReviewInterface $review): ValidationResult
    {
        $value = (string)$review->getTitle();
        $errors = [];

        if (trim($value) === '') {
            $errors[] = __('"%field" can not be empty.', ['field' => ReviewInterface::TITLE]);
        }

        return $this->validationResultFactory->create(['errors' => $errors]);
    }
}
