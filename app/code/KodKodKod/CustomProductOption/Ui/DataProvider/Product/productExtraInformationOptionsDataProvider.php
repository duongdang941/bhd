<?php
/**
 *
 * Copyright © 2022 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace KodKodKod\CustomProductOption\Ui\DataProvider\Product;


use Exception;
use KodKodKod\CustomProductOption\Ui\DataProvider\Product\Form\Modifier\ExtraInformationCustomOptions;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Model\Product\Option\Repository as ProductOptionRepository;
use Magento\Catalog\Model\Product\Option\Value as ProductOptionValueModel;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\Catalog\Ui\DataProvider\Product\ProductDataProvider;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\EntityManager\MetadataPool;
use Magento\Ui\DataProvider\AddFieldToCollectionInterface;
use Magento\Ui\DataProvider\AddFilterToCollectionInterface;
use Magento\Ui\DataProvider\Modifier\PoolInterface;

/**
 * KodKodKod\CustomProductOption\Ui\DataProvider\Product
 */
class productExtraInformationOptionsDataProvider extends ProductDataProvider
{
    /**
     * @var RequestInterface
     * @since 101.0.0
     */
    protected $request;

    /**
     * @var ProductOptionRepository
     * @since 101.0.0
     */
    protected $productOptionRepository;

    /**
     * @var ProductOptionValueModel
     * @since 101.0.0
     */
    protected $productOptionValueModel;

    /**
     * @var MetadataPool
     */
    private $metadataPool;
    /**
     * @var \KodKodKod\CustomProductOption\Model\ResourceModel\ExtraInformationCustomOptions\CollectionFactory
     */
    private \KodKodKod\CustomProductOption\Model\ResourceModel\ExtraInformationCustomOptions\CollectionFactory $extraInformationCollectionFactory;
    /**
     * @var ExtraInformationCustomOptions
     */
    private ExtraInformationCustomOptions $extraInformationCustomOptions;

    /**
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $collectionFactory
     * @param RequestInterface $request
     * @param ProductOptionRepository $productOptionRepository
     * @param ProductOptionValueModel $productOptionValueModel
     * @param \KodKodKod\CustomProductOption\Model\ResourceModel\ExtraInformationCustomOptions\CollectionFactory $extraInformationCollectionFactory
     * @param ExtraInformationCustomOptions $extraInformationCustomOptions
     * @param AddFieldToCollectionInterface[] $addFieldStrategies
     * @param AddFilterToCollectionInterface[] $addFilterStrategies
     * @param array $meta
     * @param array $data
     * @param PoolInterface|null $modifiersPool
     * @param MetadataPool|null $metadataPool
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $collectionFactory,
        RequestInterface $request,
        ProductOptionRepository $productOptionRepository,
        ProductOptionValueModel $productOptionValueModel,
        \KodKodKod\CustomProductOption\Model\ResourceModel\ExtraInformationCustomOptions\CollectionFactory $extraInformationCollectionFactory,
        ExtraInformationCustomOptions $extraInformationCustomOptions,
        array $addFieldStrategies = [],
        array $addFilterStrategies = [],
        array $meta = [],
        array $data = [],
        PoolInterface $modifiersPool = null,
        MetadataPool $metadataPool = null
    )
    {
        parent::__construct(
            $name,
            $primaryFieldName,
            $requestFieldName,
            $collectionFactory,
            $addFieldStrategies,
            $addFilterStrategies,
            $meta,
            $data,
            $modifiersPool
        );

        $this->request = $request;
        $this->extraInformationCollectionFactory = $extraInformationCollectionFactory;
        $this->extraInformationCustomOptions = $extraInformationCustomOptions;
        $this->productOptionRepository = $productOptionRepository;
        $this->productOptionValueModel = $productOptionValueModel;
        $this->metadataPool = $metadataPool ?: ObjectManager::getInstance()
            ->get(MetadataPool::class);
    }

    /**
     * @return array
     */
    public function getData()
    {
        if (!$this->getCollection()->isLoaded()) {
            $currentProductId = (int)$this->request->getParam('current_product_id');

            if (0 !== $currentProductId) {
                $this->getCollection()->getSelect()->where('e.entity_id != ?', $currentProductId);
            }

            try {
                $entityMetadata = $this->metadataPool->getMetadata(ProductInterface::class);
                $linkField = $entityMetadata->getLinkField();
            } catch (Exception $e) {
                $linkField = 'entity_id';
            }

            $this->getCollection()->getSelect()->distinct()->join(
                ['opt' => $this->getCollection()->getTable('wgt_extra_information_custom_options')],
                'opt.product_id = e.' . $linkField,
                null
            );
            $this->getCollection()->addFieldToSelect('')->load();
            $productIds = $this->getCollection()->getAllIds();
            $extraInformationCollection = $this->extraInformationCollectionFactory->create()
                ->addFieldToFilter('product_id', ['in' => $productIds]);

            /** @var ProductInterface $product */
            foreach ($this->getCollection() as $product) {
                $options = [];
                foreach ($extraInformationCollection as $extraInformationModel) {
                    $options[] = [
                        'option_id' => $extraInformationModel->getOptionId(),
                        'extra_option_name' => $extraInformationModel->getName(),
                        'values' => $this->extraInformationCustomOptions->convertDynamicValue($extraInformationModel),
                    ];
                }
                $product->setOptions($options);
            }
        }

        $items = $this->getCollection()->toArray();

        return [
            'totalRecords' => $this->getCollection()->getSize(),
            'items' => array_values($items),
        ];
    }

}
