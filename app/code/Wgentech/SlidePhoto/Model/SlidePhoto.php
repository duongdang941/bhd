<?php

namespace Wgentech\SlidePhoto\Model;

use Magento\Framework\Model\AbstractModel;
use Wgentech\SlidePhoto\Model\ResourceModel\SlidePhoto as ResourceModel;

class SlidePhoto extends AbstractModel
{
    /**
     * @var string
     */
    protected $_eventPrefix = 'wgentech_slide_photo_model';

    /**
     * @inheritdoc
     */
    protected function _construct()
    {
        $this->_init(ResourceModel::class);
    }
}
