<?php


namespace KodKodKod\Catalog\Plugin\Divante\VsbridgeIndexerCatalog\Model\Indexer\DataProvider\Product;

use Divante\VsbridgeIndexerCatalog\Model\Indexer\DataProvider\Product\AttributeData;
use KodKodKod\Catalog\Helper\Data;

class AttributeDataPlugin
{
    /**
     * @var Data
     */
    private Data $helperData;

    /**
     * @param Data $helperData
     */
    public function __construct(
        Data $helperData
    )
    {
        $this->helperData = $helperData;
    }

    /**
     * @param AttributeData $subject
     * @param $result
     * @param $indexData
     * @param $storeId
     * @return mixed
     * @throws \Exception
     */
    public function afterAddData(AttributeData $subject, $result, $indexData, $storeId)
    {
        return $this->helperData->filterTemplate($result);
    }

}
