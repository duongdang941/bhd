<?php

namespace KodKodKod\OrderCustomStatus\Plugin\Magento\Sales\Controller\AbstractController;


use Magento\Framework\App\RequestInterface;
use Magento\Sales\Controller\AbstractController\OrderViewAuthorization;
use Magento\Sales\Model\Order;

class OrderViewAuthorizationPlugin
{

    /**
     * @var RequestInterface
     */
    private $request;

    public function __construct(
        RequestInterface $request
    )
    {
        $this->request = $request;
    }

    /**
     * @param OrderViewAuthorization $subject
     * @param $result
     * @param Order $order
     * @return bool|mixed
     */
    public function afterCanView(OrderViewAuthorization $subject, $result, Order $order)
    {
        $params = $this->request->getParams();
        if (!empty($params) && !empty($params['from_vue']) && !empty($params['customer_id']) && $params['customer_id'] == $order->getCustomerId()) {
            return true;
        }
        return $result;
    }
}
