<?php

declare(strict_types=1);

namespace Shadhoc\ReviewApi\Model;

use Shadhoc\ReviewApi\Api\Data\ReviewInterface;
use Shadhoc\ReviewApi\Validation\ValidationResult;

/**
 * Responsible for Review validation
 * Extension point for base validation
 *
 * @api
 */
interface ReviewValidatorInterface
{
    /**
     * Validate Review
     *
     * @param ReviewInterface $review
     *
     * @return ValidationResult
     */
    public function validate(ReviewInterface $review): ValidationResult;
}
