<?php

namespace KodKodKod\CustomApi\Api;

/**
 * Interface ShadhocCouleurFormeInterface
 * @api
 * @since 100.0.2
 */
interface ShadhocCouleurFormeInterface
{
    /**
     * Return the couleur and forme of product by sku.
     *
     * @param string $sku
     * @return array.
     */
    public function get($sku);
}
