<?php

namespace KodKodKod\CustomProductOption\Model;

use Magento\Framework\Model\AbstractModel;
use KodKodKod\CustomProductOption\Api\Data\ProductCustomOptionValuesInterface;

class ProductCustomOptionValues extends AbstractModel implements ProductCustomOptionValuesInterface
{

    /**
     * @return mixed|string|null
     */
    public function getWgtValueDynamicImage()
    {
        return $this->_getData(self::WGT_VALUE_DYNAMIC_IMAGE);
    }

    /**
     * @param string $wgtValueDynamicImage
     * @return ProductCustomOptionValues
     */
    public function setWgtValueDynamicImage($wgtValueDynamicImage)
    {
        return $this->setData(self::WGT_VALUE_DYNAMIC_IMAGE, $wgtValueDynamicImage);
    }

    /**
     * @return mixed|null
     */
    public function getOptionCode()
    {
        return $this->_getData(self::OPTION_CODE);
    }

    /**
     * @param $optionCode
     * @return mixed|ProductCustomOptionValues
     */
    public function setOptionCode($optionCode)
    {
        return $this->setData(self::OPTION_CODE, $optionCode);
    }

    /**
     * @return mixed|null
     */
    public function getDependOn()
    {
        return $this->_getData(self::DEPEND_ON);
    }

    /**
     * @param $dependOn
     * @return mixed|ProductCustomOptionValues
     */
    public function setDependOn($dependOn)
    {
        return $this->setData(self::DEPEND_ON, $dependOn);
    }

    /**
     * @return mixed|null
     */
    public function getOptionTypeId()
    {
        return $this->_getData(self::OPTION_TYPE_ID);
    }

    /**
     * @param $optionTypeId
     * @return mixed|ProductCustomOptionValues
     */
    public function setOptionTypeId($optionTypeId)
    {
        return $this->setData(self::OPTION_TYPE_ID, $optionTypeId);
    }

    public function getWgtExtraInformation()
    {
        return $this->_getData(self::WGT_EXTRA_INFORMATION);
    }

    public function setWgtExtraInformation($extraInformation)
    {
        return $this->setData(self::WGT_EXTRA_INFORMATION, $extraInformation);
    }
}
