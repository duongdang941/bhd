<?php
declare(strict_types=1);

namespace Wgentech\CustomContact\Model;

use Magento\Contact\Model\MailInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\DataObject;
use Magento\Framework\Filesystem;
use Magento\Store\Model\StoreManagerInterface;
use Wgentech\CustomContact\Api\ContactUsManagementInterface;
use Magento\Framework\DataObjectFactory;

class ContactUsManagement implements ContactUsManagementInterface
{
    const XML_PATH_ENABLE_NOTIFY_TO_CUSTOMER = 'contact/email_contact_to_customer/enabled_notify_customer';
    const XML_PATH_EMAIL_TEMPLATE_NOTIFY_TO_CUSTOMER = 'contact/email_contact_to_customer/email_template';

    /**
     * @var MailInterface
     */
    protected $mail;
    /**
     * @var DataObjectFactory
     */
    protected $dataObjectFactory;
    /**
     * @var Filesystem
     */
    private $filesystem;
    /**
     * @var Filesystem\DriverInterface
     */
    private $fileDriver;
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;
    /**
     * @var ContactFactory
     */
    private $contactFactory;
    /**
     * @var ResourceModel\Contact
     */
    private $resourceContact;
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $scopeConfig;
    /**
     * @var MailContactCustomer
     */
    private $mailContactCustomer;

    /**
     * @param MailInterface $mail
     * @param DataObjectFactory $dataObjectFactory
     * @param Filesystem $filesystem
     * @param Filesystem\DriverInterface $fileDriver
     * @param StoreManagerInterface $storeManager
     * @param ContactFactory $contactFactory
     * @param ResourceModel\Contact $resourceContact
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param MailContactCustomer $mailContactCustomer
     */
    public function __construct(
        MailInterface                                       $mail,
        DataObjectFactory                                   $dataObjectFactory,
        Filesystem                                          $filesystem,
        \Magento\Framework\Filesystem\DriverInterface       $fileDriver,
        StoreManagerInterface                               $storeManager,
        \Wgentech\CustomContact\Model\ContactFactory        $contactFactory,
        \Wgentech\CustomContact\Model\ResourceModel\Contact $resourceContact,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Wgentech\CustomContact\Model\MailContactCustomer  $mailContactCustomer
    )
    {
        $this->mail = $mail;
        $this->dataObjectFactory = $dataObjectFactory;
        $this->filesystem = $filesystem;
        $this->fileDriver = $fileDriver;
        $this->storeManager = $storeManager;
        $this->contactFactory = $contactFactory;
        $this->resourceContact = $resourceContact;
        $this->scopeConfig = $scopeConfig;
        $this->mailContactCustomer = $mailContactCustomer;
    }

    /**
     * @param $contactForm
     * @return DataObject|\Wgentech\CustomContact\Api\Data\ContactUsInterface
     */
    public function submitForm($contactForm)
    {
        $result = $this->dataObjectFactory->create();

        if (empty($contactForm['name'])) {
            $result->setData('message', 'Enter the Name and try again.');
            return $result;
        }
        if (empty($contactForm['email'])) {
            $result->setData('message', 'Enter the Email and try again.');
            return $result;
        }
        if (false === \strpos($contactForm['email'], '@')) {
            $result->setData('message', 'The email address is invalid. Verify the email address and try again.');
            return $result;
        }
        if (empty($contactForm['comment'])) {
            $result->setData('message', 'Enter the Comment and try again.');
            return $result;
        }

        try {
            $this->sendEmail($contactForm);
            // send Email to customer
            if ($this->isEnableNotifyToCustomer()) {
                $emailTemplate = $this->getEmailTemplateContactForCustomer();
                $this->mailContactCustomer->sendEmailCustomer($contactForm, $emailTemplate);
            }
            $result->setData('message', 'Merci de nous avoir contacté. Nous reviendrons vers vous rapidement');
        } catch (LocalizedException $e) {
            $result->setData('message', $e->getMessage());
        } catch (\Exception $e) {
            $result->setData('message', 'Une erreur s"est produite lors du traitement de votre formulaire. Veuillez réessayer plus tard.');
        }
        return $result;
    }

    private function isEnableNotifyToCustomer() {
        return $this->scopeConfig->getValue(
            self::XML_PATH_ENABLE_NOTIFY_TO_CUSTOMER, \Magento\Store\Model\ScopeInterface::SCOPE_STORE) ?? false;
    }

    private function getEmailTemplateContactForCustomer() {
        return $this->scopeConfig->getValue(
                self::XML_PATH_EMAIL_TEMPLATE_NOTIFY_TO_CUSTOMER, \Magento\Store\Model\ScopeInterface::SCOPE_STORE) ?? false;
    }

    /**
     * @param array $post Post data from contact form
     * @return void
     */
    private function sendEmail($post)
    {
        $urlAttachment = $this->saveCustomFileAttachment($post);
        if (!empty($urlAttachment)) {
            unset($post['custom_file']);
            $post['attachment'] = $urlAttachment;
        }
        $this->saveContact($post);
        $this->mail->send(
            $post['email'],
            ['data' => new DataObject($post)]
        );
    }

    /**
     * Convert base 64 to file and save into media
     *
     * @param $post
     * @return string
     */
    public function saveCustomFileAttachment($post)
    {
        $urlAttachment = '';
        try {
            if (empty($post['custom_file'])) {
                return $urlAttachment;
            }
            if (empty($post['custom_file']['url']) || empty($post['custom_file']['name']) || empty($post['custom_file']['fileExtention'])) {
                return $urlAttachment;
            }
            $fileType = $post['custom_file']['fileExtention'];
            $fileName = $post['custom_file']['name'] . '.' . $fileType;
            $mediaPath = $this->filesystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA)->getAbsolutePath();
            $originalPath = 'contact/attachment/';
            $mediaFullPath = $mediaPath . $originalPath;
            if (!file_exists($mediaFullPath)) {
                mkdir($mediaFullPath, 0777, true);
            }
            /* Check File is exist or not */
            $fullFilepath = $mediaFullPath . $fileName;
            if ($this->fileDriver->isExists($fullFilepath)) {
                $fileName = rand() . time() . $fileName;
            }
            $data = $post['custom_file']['url'];
            list($type, $data) = explode(';', $data);
            list(, $data) = explode(',', $data);
            $fileContent = base64_decode($data);
            $savedFile = fopen($mediaFullPath . $fileName, "wb");
            fwrite($savedFile, $fileContent);
            fclose($savedFile);
            $urlAttachment = $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . $originalPath . $fileName;
        } catch (\Exception $exception) {
            return '';
        }
        return $urlAttachment;
    }

    /**
     * Save contact customer into database
     * @param $post
     * @return void
     */
    private function saveContact($post)
    {
        $contactModel = $this->contactFactory->create();
        try {
            $contactModel->setData($post);
            $this->resourceContact->save($contactModel);
        } catch (\Exception $exception) {

        }
    }
}
