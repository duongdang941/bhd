<?php

namespace Wgentech\SlidePhoto\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class SlidePhoto extends AbstractDb
{
    /**
     * @var string
     */
    protected $_eventPrefix = 'wgentech_slide_photo_resource_model';

    /**
     * @inheritdoc
     */
    protected function _construct()
    {
        $this->_init('wgentech_slide_photo', 'id');
        $this->_useIsObjectNew = true;
    }
}
