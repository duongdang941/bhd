<?php
 
namespace Shadhoc\CustomProduct\Model\Product\Type;
 
class CustomProductType extends \Magento\Bundle\Model\Product\Type {

    const TYPE_ID= 'custom_product_type';

    /**
     * {@inheritdoc}
     */
    public function deleteTypeSpecificData(\Magento\Catalog\Model\Product $product)
    {
        // method intentionally empty
    }
}