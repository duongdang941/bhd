<?php

declare(strict_types=1);

namespace KodKodKod\CustomVsbridgeIndexerCatalog\Plugin\Divante\VsbridgeIndexerCatalog\Model\Indexer\DataProvider\Product;

use Divante\VsbridgeIndexerCatalog\Api\ArrayConverter\Product\CustomOptionConverterInterface;
use Divante\VsbridgeIndexerCatalog\Model\Indexer\DataProvider\Product\CustomOptions as VsbridgeIndexerCatalogCustomOptions;
use Divante\VsbridgeIndexerCatalog\Model\ProductMetaData;
use Divante\VsbridgeIndexerCatalog\Model\ResourceModel\Product\CustomOptions as Resource;
use Divante\VsbridgeIndexerCatalog\Model\ResourceModel\Product\CustomOptionValues as OptionValuesResource;
use KodKodKod\CustomVsbridgeIndexerCatalog\Helper\HelperOption;

class CustomOptions extends VsbridgeIndexerCatalogCustomOptions
{
    /**
     * @var ProductMetaData
     */
    private ProductMetaData $productMetaData;
    /**
     * @var OptionValuesResource
     */
    private OptionValuesResource $optionValuesResourceModel;
    /**
     * @var Resource
     */
    private Resource $optionsResourceModel;
    /**
     * @var CustomOptionConverterInterface
     */
    private CustomOptionConverterInterface $productOptionProcessor;
    /**
     * @var HelperOption
     */
    private HelperOption $helperOption;

    /**
     * @param Resource $resource
     * @param OptionValuesResource $customOptionValues
     * @param CustomOptionConverterInterface $processor
     * @param ProductMetaData $productMetaData
     * @param HelperOption $helperOption
     */
    public function __construct(
        Resource                       $resource,
        OptionValuesResource           $customOptionValues,
        CustomOptionConverterInterface $processor,
        ProductMetaData                $productMetaData,
        HelperOption $helperOption
    )
    {
        $this->productMetaData = $productMetaData;
        $this->optionValuesResourceModel = $customOptionValues;
        $this->optionsResourceModel = $resource;
        $this->productOptionProcessor = $processor;
        $this->helperOption = $helperOption;
        parent::__construct($resource, $customOptionValues, $processor, $productMetaData);
    }

    /**
     * @inheritdoc
     */
    public function addData(array $indexData, $storeId)
    {
        $storeId = (int)$storeId;
        $linkField = $this->productMetaData->get()->getLinkField();
        $linkFieldIds = array_column($indexData, $linkField);

        $options = $this->helperOption->loadProductOptions($linkFieldIds, $storeId);
        if (empty($options)) {
            return $indexData;
        }

        $optionIds = array_column($options, 'option_id');
        $values = $this->optionValuesResourceModel->loadOptionValues($optionIds, $storeId);

        $optionsByProduct = $this->productOptionProcessor->process($options, $values);


        foreach ($indexData as $productId => $productData) {
            $linkFieldValue = $productData[$linkField];

            if (isset($optionsByProduct[$linkFieldValue])) {
                $result = $this->prepareCustomOption($optionsByProduct[$linkFieldValue]);
                $indexData[$productId]['custom_options'] = $result;
            }
        }

        return $indexData;
    }

    /**
     * @param $options
     * @return array
     */
    private function prepareCustomOption($options)
    {
        $result = [];
        foreach ($options as $option) {
            $data = [];
            $data['extension_attributes']['wgt_common_image'] = $option['wgt_common_image'] ?? null;
            $data['extension_attributes']['wgt_common_question_code'] = $option['wgt_common_question_code'] ?? null;
            $data['extension_attributes']['wgt_common_depend_on'] = $option['wgt_common_depend_on'] ?? null;
            if ($option['wgt_common_extra_information'] == 0) {
                $data['extension_attributes']['wgt_common_extra_information'] = null;
            } else {
                $data['extension_attributes']['wgt_common_extra_information'] = $option['wgt_common_extra_information'];
            }
            $data['extension_attributes']['wgt_template_id'] = $option['wgt_template_id'] ?? null;
            $data['extension_attributes']['wgt_formulated_option'] = $option['wgt_formulated_option'] ?? null;
            $data['extension_attributes']['wgt_sort_order'] = $option['wgt_sort_order'] ?? null;
            $data['extension_attributes']['wgt_option_required'] = $option['wgt_option_required'] ?? null;
            $data['extension_attributes']['wgt_answer_option'] = $option['wgt_answer_option'] ?? null;
            $result[] = array_merge($data, $this->ignoreField($option));
        }
        return $result;
    }

    /**
     * @param $option
     * @return mixed
     */
    private function ignoreField($option)
    {
        unset($option['wgt_common_image']);
        unset($option['wgt_common_question_code']);
        unset($option['wgt_common_depend_on']);
        unset($option['wgt_common_extra_information']);
        unset($option['wgt_template_id']);
        unset($option['wgt_formulated_option']);
        unset($option['wgt_sort_order']);
        unset($option['wgt_option_required']);
        unset($option['wgt_answer_option']);
        return $option;
    }
}
