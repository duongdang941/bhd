<?php

namespace Wgentech\SlidePhoto\Ui\Component\Listing\Column;

class IsActive extends \Magento\Ui\Component\Listing\Columns\Column implements \Magento\Framework\Data\OptionSourceInterface
{

    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as $key => $item) {
                if(isset($item['is_active']))
                {
                    $is_active = $item['is_active'];
                    if($is_active == true){
                        $dataSource['data']['items'][$key]['is_active'] = 'Active';
                    }
                    else{
                        $dataSource['data']['items'][$key]['is_active'] = 'Inactive';
                    }
                }
            }
        }
        return $dataSource;
    }

    public function toOptionArray()
    {
        return [
            ['label' => __('Active'), 'value' => 1],
            ['label' => __('Inactive'), 'value' => 0],
        ];
    }
}