<?php

namespace KodKodKod\Catalog\Model;

use Magento\Framework\App\ObjectManager;
use KodKodKod\Catalog\Helper\Data;

class Product extends \Magento\Catalog\Model\Product
{
    public function getAvailabilityLabel()
    {
        $dataHelper = ObjectManager::getInstance()->get(Data::class);
        $deliveryTime = $dataHelper->prepareAttributeAvailabilityLabel($this);
        return $deliveryTime;
    }
}
