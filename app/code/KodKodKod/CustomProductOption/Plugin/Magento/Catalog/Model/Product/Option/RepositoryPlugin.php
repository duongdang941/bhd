<?php
namespace KodKodKod\CustomProductOption\Plugin\Magento\Catalog\Model\Product\Option;


use Magento\Catalog\Api\Data\ProductCustomOptionInterface;
use Magento\Catalog\Model\Product\Option\Repository;

class RepositoryPlugin
{

    /**
     * @param Repository $subject
     * @param ProductCustomOptionInterface $option
     * @return array
     */
    public function beforeSave(Repository $subject, ProductCustomOptionInterface $option)
    {
        $option = $this->convertOptionImageArrayToString($option);
        $wgtCommonImage = $option->getData('wgt_common_image');
        if (!empty($wgtCommonImage) && is_array($wgtCommonImage)) {
            $wgtCommonImageNewValue = $wgtCommonImage[0]['name'] ?? null;
            $option->setData('wgt_common_image', $wgtCommonImageNewValue);
        }
        if (is_null($wgtCommonImage)) {
            $option->setData('wgt_common_image', '');
        }

        return [$option];
    }

    /**
     * @param $option
     * @return mixed
     */
    private function convertOptionImageArrayToString($option)
    {
        $values = $option->getData('values');
        if (!$values) {
            return $option;
        }

        foreach ($values as $key => $value) {
            $wgtValueDynamicImageValue = null;
            $wgtValueDynamicImageArray = $value['wgt_value_dynamic_image'] ?? [];

            if (is_array($wgtValueDynamicImageArray)) {
                $wgtValueDynamicImageValue = $wgtValueDynamicImageArray[0]['name'] ?? null;
            }

            if (is_string($wgtValueDynamicImageArray)) {
                $wgtValueDynamicImageValue = $wgtValueDynamicImageArray;
            }

            $values[$key]['wgt_value_dynamic_image'] = $wgtValueDynamicImageValue;
        }

        $option->setData('values', $values);

        return $option;
    }
}
