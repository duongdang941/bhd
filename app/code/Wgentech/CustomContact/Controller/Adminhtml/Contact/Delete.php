<?php

namespace Wgentech\CustomContact\Controller\Adminhtml\Contact;

use Wgentech\CustomContact\Controller\Adminhtml\Contact;
use Wgentech\CustomContact\Model\ContactFactory;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Registry;

class Delete extends Contact
{
    /**
     * @var SlidePhotoFactory
     */
    protected $slidePhotoFactory;
    /**
     * @var ContactFactory
     */
    private $contactFactory;

    /**
     * @param Context $context
     * @param Registry $coreRegistry
     * @param ContactFactory $contactFactory
     */
    public function __construct(
        Context        $context,
        Registry       $coreRegistry,
        ContactFactory $contactFactory
    )
    {
        $this->contactFactory = $contactFactory;
        parent::__construct($context, $coreRegistry);
    }

    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();

        $id = $this->getRequest()->getParam('id');
        if ($id) {
            try {
                $model = $this->contactFactory->create();
                $model->load($id);
                $model->delete();
                $this->messageManager->addSuccessMessage(__('You deleted the Contact.'));

                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                // display error message
                $this->messageManager->addErrorMessage($e->getMessage());
                // go back to edit form
                return $resultRedirect->setPath('*/*/edit', ['id' => $id]);
            }
        }
        // display error message
        $this->messageManager->addErrorMessage(__('We can\'t find the contact to delete.'));
        // go to grid
        return $resultRedirect->setPath('*/*/');
    }

}
