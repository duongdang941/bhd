<?php

namespace Wgentech\SlidePhoto\Api;

interface SlidePhotoInterface
{
    /**
     * @return mixed
     */
    public function getList();
}
