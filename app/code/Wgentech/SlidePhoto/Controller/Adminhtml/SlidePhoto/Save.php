<?php
namespace Wgentech\SlidePhoto\Controller\Adminhtml\SlidePhoto;

use Magento\Framework\Exception\LocalizedException;
use Wgentech\SlidePhoto\Model\SlidePhotoFactory;
use Wgentech\SlidePhoto\Model\ResourceModel\SlidePhotoFactory as ResourceSlidePhoto;

class Save extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\Framework\App\Request\DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @var SlidePhotoFactory
     */
    protected $modelSlidePhoto;

    /**
     * @var ResourceSlidePhoto
     */
    protected $resourceSlidePhoto;

    public function __construct(
        SlidePhotoFactory $modelSlidePhoto,
        ResourceSlidePhoto $resourceSlidePhoto,
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
    ) {
        $this->modelSlidePhoto = $modelSlidePhoto;
        $this->resourceSlidePhoto = $resourceSlidePhoto;
        $this->dataPersistor = $dataPersistor;
        parent::__construct($context);
    }
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();

        $data = $this->getRequest()->getPostValue();

        if($data){
            $id = $this->getRequest()->getParam('id');
            $resourceSlidePhoto = $this->resourceSlidePhoto->create();
            $modelSlidePhoto = $this->modelSlidePhoto->create()->load($id);

            if (!$modelSlidePhoto->getId() && $id) {
                $this->messageManager->addErrorMessage(__('This Slide Photo no longer exists.'));
                return $resultRedirect->setPath('*/*/');
            }

            $data['image'] = $data['images'][0]['name'];

            $modelSlidePhoto->setData($data);

            try {
                $resourceSlidePhoto->save($modelSlidePhoto);

                $this->messageManager->addSuccessMessage(__('You saved the Slide Photo.'));
                $this->dataPersistor->clear('wgentech_slide_photo');

                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['id' => $modelSlidePhoto->getId()]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the Slide Photo.'));
            }

            $this->dataPersistor->set('wgentech_slide_photo', $data);
            return $resultRedirect->setPath('*/*/edit', ['id' => $this->getRequest()->getParam('id')]);
        }
        return $resultRedirect->setPath('*/*/');

    }

}
