<?php

namespace Wgentech\SlidePhoto\Api\Data;

interface SlidePhotoInterface
{
    /**
     * String constants for property names
     */
    const ID = "id";
    const IMAGE = "image";
    const TITLE = "title";
    const TEXT = "text";
    const URL = "url";
    const IS_ACTIVE = "is_active";
    const BUTTON_TITLE = "button_title";

    /**
     * Getter for IsActive.
     *
     * @return bool|null
     */
    public function getIsActive(): ?bool;

    /**
     * Setter for IsActive.
     *
     * @param bool|null $isActive
     *
     * @return void
     */
    public function setIsActive(?bool $isActive): void;

    /**
     * Getter for Id.
     *
     * @return int|null
     */
    public function getId(): ?int;

    /**
     * Setter for Id.
     *
     * @param int|null $id
     *
     * @return void
     */
    public function setId(?int $id): void;

    /**
     * Getter for Image.
     *
     * @return string|null
     */
    public function getImage(): ?string;

    /**
     * Setter for Image.
     *
     * @param string|null $image
     *
     * @return void
     */
    public function setImage(?string $image): void;

    /**
     * Getter for Title.
     *
     * @return string|null
     */
    public function getTitle(): ?string;

    /**
     * Setter for Title.
     *
     * @param string|null $title
     *
     * @return void
     */
    public function setTitle(?string $title): void;

    /**
     * Getter for Text.
     *
     * @return string|null
     */
    public function getText(): ?string;

    /**
     * Setter for Text.
     *
     * @param string|null $text
     *
     * @return void
     */
    public function setText(?string $text): void;

    /**
     * Getter for Url.
     *
     * @return string|null
     */
    public function getUrl(): ?string;

    /**
     * Setter for Url.
     *
     * @param string|null $url
     *
     * @return void
     */
    public function setUrl(?string $url): void;


    /**
     * Getter for Text.
     *
     * @return string|null
     */
    public function getButtonTitle(): ?string;

    /**
     * Setter for Text.
     *
     * @param string|null $buttonTitle
     *
     * @return void
     */
    public function setButtonTitle(?string $buttonTitle): void;
}
