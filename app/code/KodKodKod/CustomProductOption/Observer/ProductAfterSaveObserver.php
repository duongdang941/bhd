<?php
/**
 *
 * @copyright Copyright © 2021 Wgentech. All rights reserved.
 * @author    linh.pv68@gmail.com
 */

namespace KodKodKod\CustomProductOption\Observer;

use KodKodKod\CustomProductOption\Ui\DataProvider\Product\Form\Modifier\ExtraInformationCustomOptions;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\InventoryReservationsApi\Model\ReservationInterface;

/**
 * KodKodKod\CustomProductOption\Observer
 */
class ProductAfterSaveObserver implements ObserverInterface
{
    /**
     * @var \KodKodKod\CustomProductOption\Model\ResourceModel\ExtraInformationCustomOptions
     */
    private \KodKodKod\CustomProductOption\Model\ResourceModel\ExtraInformationCustomOptions $extraInformationResourceModel;

    /**
     * @var \Magento\Framework\Serialize\SerializerInterface
     */
    private \Magento\Framework\Serialize\SerializerInterface $serialize;

    /**
     * @var array
     */
    private array $listOptionIdNeedRemove;

    /**
     * ProductAfterSaveObserver constructor.
     *
     * @param \KodKodKod\CustomProductOption\Model\ResourceModel\ExtraInformationCustomOptions $extraInformationResourceModel
     * @param \Magento\Framework\Serialize\SerializerInterface $serialize
     */
    public function __construct(
        \KodKodKod\CustomProductOption\Model\ResourceModel\ExtraInformationCustomOptions $extraInformationResourceModel,
        \Magento\Framework\Serialize\SerializerInterface $serialize
    )
    {
        $this->serialize = $serialize;
        $this->extraInformationResourceModel = $extraInformationResourceModel;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        /** @var \Magento\Catalog\Controller\Adminhtml\Product\Save $controller */
        $controller = $observer->getEvent()->getData('controller');
        $product = $observer->getEvent()->getData('product');
        $productRequestData = $controller->getRequest()->getPostValue('product');

        if (empty($controller) || empty($product)) {
            return;
        }
        $connection = $this->extraInformationResourceModel->getConnection();
        $tableName = $connection->getTableName('wgt_extra_information_custom_options');
        $defaultExtraInformation[] = [
            'product_id' => $product->getId(),
            'name' => 'Default Extra Information',
            'dynamic_value' => null,
            'sort_order' => "0"
        ];
        if (isset($productRequestData[ExtraInformationCustomOptions::GRID_VALUES_OPTIONS_NAME])
            && $productRequestData[ExtraInformationCustomOptions::GRID_VALUES_OPTIONS_NAME] != null
        ) {
            $extraInformationValues = $productRequestData[ExtraInformationCustomOptions::GRID_VALUES_OPTIONS_NAME];
            $insertData = $this->prepareDataInsert($extraInformationValues, $product->getId());
            $optionDeleteIds = $this->prepareOptionToRemove($connection, $tableName, $product->getId(), $insertData);

            try {
                $connection->beginTransaction();
                $connection->insertOnDuplicate($tableName, $insertData);
                if (!empty($optionDeleteIds)) {
                    $condition = ['option_id IN (?)' => $optionDeleteIds];
                    $connection->delete($tableName, $condition);
                }
                $connection->commit();
            } catch (\Exception $e) {
                $connection->rollBack();
            }
            //handle case delete all extra information
        } else {
            try {
                $connection->beginTransaction();
                $connection->insertOnDuplicate($tableName, $defaultExtraInformation);
                $select = $connection->select()->from($tableName, 'option_id')
                    ->where("product_id=?", $product->getId());
                $resultIds = $connection->fetchCol($select);
                if (!empty($resultIds)) {
                    $condition = ['option_id IN (?)' => $resultIds];
                    $connection->delete($tableName, $condition);
                    $connection->commit();
                }
            }catch (\Exception $e) {
                $connection->rollBack();
            }
        }
    }

    /**
     * @param $connection
     * @param $tableName
     * @param $entityId
     * @param $insertData
     * @return array
     */
    private function prepareOptionToRemove($connection, $tableName, $entityId, $insertData)
    {

        $optionInsertIds = [];
        $select = $connection->select()->from($tableName, 'option_id')
            ->where("product_id=?", $entityId);
        $result = $connection->fetchCol($select);
        if (!empty($insertData)) {
            foreach ($insertData as $item) {
                $optionInsertIds[] = $item['option_id'];
            }
        }
        return array_diff($result, $optionInsertIds);
    }


    /**
     * @param $extraInformationValues
     * @param $productId
     * @return array
     */
    private function prepareDataInsert($extraInformationValues, $productId)
    {
        $insertData = [];
        foreach ($extraInformationValues as $extraInformationValue) {
            $optionId = $extraInformationValue['option_id'] ?? null;
            $insertData[] = [
                'option_id' => $optionId,
                'product_id' => $productId,
                'name' => $extraInformationValue['extra_option_name'] ?? null,
                'dynamic_value' => $this->getDynamicValue($extraInformationValue)
            ];
        }

        return $insertData;
    }

    /**
     * @param $extraInformationValue
     * @return bool|string|null
     */
    protected function getDynamicValue($extraInformationValue)
    {
        $dynamicValue = $extraInformationValue['values'] ?? null;

        if ($dynamicValue != null) {
            $dynamicValue = $this->serialize->serialize($dynamicValue);
        }

        return $dynamicValue;
    }
}
