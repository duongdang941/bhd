<?php

namespace KodKodKod\CustomApi\Model;

use Magento\Catalog\Api\ProductRepositoryInterface;

class CouleurForme implements \KodKodKod\CustomApi\Api\ShadhocCouleurFormeInterface
{
    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @param ProductRepositoryInterface $productRepository
     */
    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepository =  $productRepository;
    }

    public function get($sku): array
    {
        $product = $this->productRepository->get($sku);
        $forme_voile = $product->getResource()->getAttribute('forme_voile')->getFrontend()->getValue($product);
        $couleur_voile = $product->getResource()->getAttribute('couleur_voile')->getFrontend()->getValue($product);

        return [$forme_voile, $couleur_voile];
    }
}
