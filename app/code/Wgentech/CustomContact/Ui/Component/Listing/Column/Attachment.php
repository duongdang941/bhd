<?php

namespace Wgentech\CustomContact\Ui\Component\Listing\Column;
use Magento\Ui\Component\Listing\Columns\Column;

class Attachment extends Column
{
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as $key => $item) {
                if (isset($item['attachment'])) {
                    $dataSource['data']['items'][$key]['attachment'] = '<a href="' . $item['attachment'] . '" target="_blank">' . __('Download') . '</a>';
                } else {
                    $dataSource['data']['items'][$key]['attachment'] = '';
                }
            }
        }

        return $dataSource;
    }
}
