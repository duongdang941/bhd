<?php

namespace KodKodKod\Catalog\Helper;

use KodKodKod\Catalog\Setup\Patch\Data\AddAvailabilityLabelAttribute;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Data\Collection;
use Magento\Widget\Model\Template\FilterEmulate;
use Magento\Catalog\Model\ProductRepository;

class Data extends AbstractHelper
{
    const LEVEL_CATEGORY = 'level';
    const WGT_AVAILABILITY_LABEL_DEFAULT = 'En rupture de stock';
    const ATTRIBUTE_DESCRIPTION = 'description';
    /**
     * @var FilterEmulate
     */
    private FilterEmulate $filterEmulate;
    /**
     * @var ProductRepository
     */
    private $productRepository;

    /**
     * @param Context $context
     * @param FilterEmulate $filterEmulate
     * @param ProductRepository $productRepository
     */
    public function __construct(
        Context           $context,
        FilterEmulate     $filterEmulate,
        ProductRepository $productRepository
    )
    {
        parent::__construct($context);
        $this->filterEmulate = $filterEmulate;
        $this->productRepository = $productRepository;
    }

    /**
     * @param $data
     * @throws \Exception
     */
    public function filterTemplate($data)
    {
        foreach ($data as $entityId => $productData) {
            $description = $productData[self::ATTRIBUTE_DESCRIPTION] ?? '';
            $productData[self::ATTRIBUTE_DESCRIPTION] = $this->filterEmulate->filter($description);
            $resultLabel = $this->filterForLabel($productData);
            $productData[AddAvailabilityLabelAttribute::ATTRIBUTE_AVAILABILITY_LABEL] = $resultLabel;
            $data[$entityId] = $productData;
        }

        return $data;
    }

    private function filterForLabel($productData)
    {
        $sku = $productData['sku'] ?? '';
        if (!$sku) {
            return '';
        }
        try {
            $entity = $this->productRepository->get($sku);
            $result = $this->prepareAttributeAvailabilityLabel($entity);

        } catch (\Exception $exception) {
            return '';
        }
        return $result;
    }


    public function prepareAttributeAvailabilityLabel($entity)
    {
        $attributeAvailability = $entity->getData(AddAvailabilityLabelAttribute::ATTRIBUTE_AVAILABILITY_LABEL) ?? '';

        if (empty($attributeAvailability)) {
            $arrLabels = [];
            $categoriesParent = $entity->getCategoryCollection()
                ->addAttributeToSelect(AddAvailabilityLabelAttribute::ATTRIBUTE_AVAILABILITY_LABEL)
                ->addOrder(self::LEVEL_CATEGORY, Collection::SORT_ORDER_ASC);

            foreach ($categoriesParent as $category) {
                $attributeAvailabilityCategory = $category->getData(AddAvailabilityLabelAttribute::ATTRIBUTE_AVAILABILITY_LABEL);
                if (!empty($attributeAvailabilityCategory)) {
                    $arrLabels[] = $attributeAvailabilityCategory;
                }
            }
            if (empty($arrLabels)) {
                return self::WGT_AVAILABILITY_LABEL_DEFAULT;
            }
            $attributeAvailability = array_pop($arrLabels);
        }
        return $attributeAvailability;
    }
}
