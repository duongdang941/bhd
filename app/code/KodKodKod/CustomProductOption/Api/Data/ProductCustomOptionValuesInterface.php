<?php
/**
 * ProductCustomOptionValuesInterface
 *
 * @copyright Copyright © 2021 Wgentech. All rights reserved.
 */

namespace KodKodKod\CustomProductOption\Api\Data;

interface ProductCustomOptionValuesInterface
{
    const WGT_VALUE_DYNAMIC_IMAGE = 'wgt_value_dynamic_image';
    const OPTION_CODE = 'option_code';
    const DEPEND_ON = 'depend_on';
    const OPTION_TYPE_ID = 'option_type_id';
    const WGT_EXTRA_INFORMATION = 'wgt_extra_information';

    /**
     * @return string
     */
    public function getWgtValueDynamicImage();

    /**
     *
     * @param string $wgtValueDynamicImage
     * @return $this
     */
    public function setWgtValueDynamicImage($wgtValueDynamicImage);

    /**
     * @return mixed
     */
    public function getOptionCode();

    /**
     * @param $optionCode
     * @return mixed
     */
    public function setOptionCode($optionCode);

    /**
     * @return mixed
     */
    public function getDependOn();

    /**
     * @param $dependOn
     * @return mixed
     */
    public function setDependOn($dependOn);

    /**
     * @return mixed
     */
    public function getOptionTypeId();

    /**
     * @param $optionTypeId
     * @return mixed
     */
    public function setOptionTypeId($optionTypeId);

    /**
     * @return mixed
     */
    public function getWgtExtraInformation();

    /**
     * @param $extraInformation
     * @return mixed
     */
    public function setWgtExtraInformation($extraInformation);

}
