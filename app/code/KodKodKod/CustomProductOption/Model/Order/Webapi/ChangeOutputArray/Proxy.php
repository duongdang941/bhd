<?php
/**
 *
 * @copyright Copyright © 2021 Wgentech. All rights reserved.
 * @author    linh.pv68@gmail.com
 */

namespace KodKodKod\CustomProductOption\Model\Order\Webapi\ChangeOutputArray;

use KodKodKod\CustomProductOption\Api\Data\ProductCustomOptionValuesInterface;
use KodKodKod\CustomProductOption\Model\ExtraInformationCustomOptionsFactory;
use KodKodKod\CustomProductOption\Model\ResourceModel\ExtraInformationCustomOptions;
use Magento\Catalog\Api\Data\ProductCustomOptionInterface;
use Magento\Framework\Serialize\SerializerInterface;

/**
 * KodKodKod\CustomProductOption\Model\Order\Webapi\ChangeOutputArray
 */
class Proxy
{
    private \KodKodKod\CustomProductOption\Helper\Data $helperData;

    /**
     * Proxy constructor.
     *
     * @param \KodKodKod\CustomProductOption\Helper\Data $helperData
     */
    public function __construct(
        \KodKodKod\CustomProductOption\Helper\Data $helperData
    ) {
        $this->helperData = $helperData;
    }

    /**
     * @param $dataObject
     * @param array $result
     * @return array
     */
    public function execute($dataObject, array $result)
    {
        if ($dataObject instanceof ProductCustomOptionInterface) {
            $wgtCommonExtraInformationId = $result['extension_attributes']['wgt_common_extra_information'] ?? null;

            if ($wgtCommonExtraInformationId != null || $wgtCommonExtraInformationId != 0) {
                $result['extension_attributes']['wgt_common_extra_information'] = $this->helperData->getExtraInformationCustomOptions($wgtCommonExtraInformationId);
            }
        }

        if ($dataObject instanceof ProductCustomOptionValuesInterface) {
            $wgtExtraInformationId = $result['wgt_extra_information'] ?? null;

            if ($wgtExtraInformationId != null || $wgtExtraInformationId != 0) {
                $result['wgt_extra_information'] = $this->helperData->getExtraInformationCustomOptions($wgtExtraInformationId);
            }
        }

        return $result;
    }


}
