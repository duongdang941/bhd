<?php

namespace Wgentech\CustomContact\Controller\Adminhtml;

use Magento\Backend\App\Action\Context;
use Magento\Framework\Registry;

abstract class Contact extends \Magento\Backend\App\Action
{
    const ADMIN_RESOURCE = 'Wgentech_CustomContact::menu';

    /**
     * @var Registry
     */
    protected $_coreRegistry;

    /**
     * @param Context $context
     * @param Registry $coreRegistry
     */
    public function __construct(
        Context  $context,
        Registry $coreRegistry
    )
    {
        $this->_coreRegistry = $coreRegistry;
        parent::__construct($context);
    }


    public function initPage($resultPage)
    {
        $resultPage->setActiveMenu(self::ADMIN_RESOURCE)
            ->addBreadcrumb(__('Wgentech'), __('Wgentech'))
            ->addBreadcrumb(__('Manage Contact'), __('Manage Contact'));
        return $resultPage;
    }
}
