<?php

namespace Wgentech\CustomContact\Plugin\Magento\Customer\Controller\Account;

use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Controller\Account\CreatePassword;
use Magento\Customer\Model\ForgotPasswordToken\GetCustomerByToken;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\Result\RedirectFactory;

class CreatePasswordPlugin
{
    /**
     * @var AccountManagementInterface
     */
    private AccountManagementInterface $accountManagement;
    /**
     * @var RedirectFactory
     */
    private RedirectFactory $resultRedirectFactory;
    /**
     * @var ScopeConfigInterface
     */
    private ScopeConfigInterface $scopeConfig;
    /**
     * @var GetCustomerByToken
     */
    private GetCustomerByToken $getByToken;

    public function __construct(
        Context                    $context,
        GetCustomerByToken         $getByToken,
        AccountManagementInterface $accountManagement,
        ScopeConfigInterface       $scopeConfig
    )
    {
        $this->getByToken = $getByToken;
        $this->accountManagement = $accountManagement;
        $this->resultRedirectFactory = $context->getResultRedirectFactory();
        $this->scopeConfig = $scopeConfig;

    }

    /**
     * @param CreatePassword $subject
     * @param callable $proceed
     * @return Redirect
     */
    public function aroundExecute(CreatePassword $subject, callable $proceed)
    {

        $resetPasswordToken = (string)$subject->getRequest()->getParam('token');
        $resultRedirect = $this->resultRedirectFactory->create();
        try {
            $this->accountManagement->validateResetPasswordLinkToken(null, $resetPasswordToken);
            $customer = $this->getByToken->execute($resetPasswordToken);
            $customerEmail = $customer->getEmail();
            $urlRedirect = $this->getVueUrlRedirect() . '/?token=' . $resetPasswordToken . '&email=' . $customerEmail . '&is_reset=success';
            $resultRedirect->setPath($urlRedirect, []);
            return $resultRedirect;
        } catch (\Exception $exception) {
            $urlRedirect = $this->getVueUrlRedirect() . '/?is_reset=error';
            $resultRedirect->setPath($urlRedirect, []);
            return $resultRedirect;
        }

    }

    private function getVueUrlRedirect()
    {
        return $this->scopeConfig->getValue(\Lyranetwork\Payzen\Controller\Payment\Response::VUESTOREFRONT_CONFIG_PATH);
    }
}
