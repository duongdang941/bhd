<?php
/**
 *
 * Copyright © 2022 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace KodKodKod\CustomApi\Plugin\Magento\Catalog\Model\ProductLink\Converter;


use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ProductLink\Converter\DefaultConverter;
use KodKodKod\CustomProductOption\Helper\Data;
/**
 * KodKodKod\CustomApi\Plugin\Magento\Catalog\Model\ProductLink\Converter
 */
class DefaultConverterPlugin
{
    /**
     * @var Data
     */
    private Data $imageHelper;

    public function __construct(
        Data $imageHelper
    ){
        $this->imageHelper = $imageHelper;
    }

    /**
     * @param DefaultConverter $subject
     * @param array $result
     * @param Product $product
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function afterConvert(DefaultConverter $subject, array $result, Product $product): array
    {
        $result['custom_attributes'][] = $this->addPriceValue($product);
        $result['custom_attributes'][] = $this->addImage($product);

        return $result;
    }

    /**
     * @param $product
     * @return array
     */
    private function addPriceValue($product)
    {
        return [
            'attribute_code' => 'price',
            'value' => (float)$product->getData('price')
        ];
    }

    /**
     * @param $product
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    private function addImage($product)
    {
        return [
            'attribute_code' => 'image',
            'value' => $this->imageHelper->getProductImageUrl($product)
        ];
    }
}
