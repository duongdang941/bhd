<?php

namespace KodKodKod\OrderCustomStatus\Setup\Patch\Data;

use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Status;
use Magento\Sales\Model\Order\StatusFactory;
use Magento\Sales\Model\ResourceModel\Order\Status as StatusResource;
use Magento\Sales\Model\ResourceModel\Order\StatusFactory as StatusResourceFactory;

class OrderStatus implements DataPatchInterface
{
    const IN_PROCESS_STATUS_CODE = 'in_process';
    const IN_PROCESS_STATUS_LABEL = 'In Process';

    const IN_PREPARATION_STATUS_CODE = 'in_preparation';
    const IN_PREPARATION_STATUS_LABEL = 'In Preparation';

    const SHIPPED_PARTIALLY_STATUS_CODE = 'shipped_partially';
    const SHIPPED_PARTIALLY_STATUS_LABEL = 'Shipped Partially';

    const ORDER_SHIPPED_STATUS_CODE = 'shipped';
    const ORDER_SHIPPED_STATUS_LABEL = 'Shipped';

    /**
     * @var StatusFactory
     */
    protected $statusFactory;

    /**
     * @var StatusResourceFactory
     */
    protected $statusResourceFactory;

    /**
     * @param StatusFactory $statusFactory
     * @param StatusResourceFactory $statusResourceFactory
     */
    public function __construct(
        StatusFactory         $statusFactory,
        StatusResourceFactory $statusResourceFactory
    )
    {
        $this->statusFactory = $statusFactory;
        $this->statusResourceFactory = $statusResourceFactory;
    }

    /**
     * @inheritdoc
     */
    public function apply()
    {
        /** @var StatusResource $statusResource */
        $statusResource = $this->statusResourceFactory->create();
        /** @var Status $status */
        $data = [
            [
                'status' => self::IN_PROCESS_STATUS_CODE,
                'label' => self::IN_PROCESS_STATUS_LABEL,
            ],
            [
                'status' => self::IN_PREPARATION_STATUS_CODE,
                'label' => self::IN_PREPARATION_STATUS_LABEL
            ],
            [
                'status' => self::SHIPPED_PARTIALLY_STATUS_CODE,
                'label' => self::SHIPPED_PARTIALLY_STATUS_LABEL
            ],
            [
                'status' => self::ORDER_SHIPPED_STATUS_CODE,
                'label' => self::ORDER_SHIPPED_STATUS_LABEL
            ]
        ];

        foreach ($data as $item) {
            $status = $this->statusFactory->create();
            $status->setData($item);
            try {
                $statusResource->save($status);
            } catch (\Exception $exception) {
                return;
            }
            $status->assignState(Order::STATE_PROCESSING, true, true);
        }

        return $this;
    }

    /**
     * @inheritdoc
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function getAliases()
    {
        return [];
    }
}
