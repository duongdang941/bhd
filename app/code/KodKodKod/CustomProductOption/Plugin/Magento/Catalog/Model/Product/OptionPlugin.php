<?php

namespace KodKodKod\CustomProductOption\Plugin\Magento\Catalog\Model\Product;

use Magento\Catalog\Model\Product\Option;

class OptionPlugin
{
    const DEFAULT_MAX_CHARACTER = 99999999;

    /**
     * @param Option $subject
     * @param $result
     * @return int
     */
    public function afterGetMaxCharacters(Option $subject, $result)
    {
        return self::DEFAULT_MAX_CHARACTER;
    }
}
