<?php
/**
 *
 * Copyright © 2022 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace KodKodKod\CustomProductOption\Plugin\Divante\VsbridgeIndexerCatalog\Model\ResourceModel\Product;


use Divante\VsbridgeIndexerCatalog\Model\ResourceModel\Product\CustomOptions;
use Divante\VsbridgeIndexerCatalog\Model\ResourceModel\Product\CustomOptionValues;

class CustomOptionValuesPlugin
{
    private \KodKodKod\CustomProductOption\Helper\Data $helperData;

    public function __construct(
        \KodKodKod\CustomProductOption\Helper\Data $helperData
    )
    {
        $this->helperData = $helperData;
    }

    /**
     * @param CustomOptionValues $subject
     * @param array $result
     * @param array $optionIds
     * @param int $storeId
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function afterLoadOptionValues(CustomOptionValues $subject, array $result, array $optionIds, int $storeId): array
    {
        foreach ($result as $optionValueId  => $optionValue) {
            $wgtExtraInformation = $optionValue['wgt_extra_information'] ?? null;
            $wgtValueDynamicImage = $optionValue['wgt_value_dynamic_image'] ?? null;
            if ($wgtExtraInformation) {
                $result[$optionValueId]['wgt_extra_information'] = $this->helperData->getExtraInformationCustomOptions($wgtExtraInformation);
            }
            if ($wgtValueDynamicImage) {
                $result[$optionValueId]['wgt_value_dynamic_image'] = $this->helperData->getWgtMediaUrl($wgtValueDynamicImage);
            }
            if ($wgtExtraInformation == 0) {
                $result[$optionValueId]['wgt_extra_information'] = null;
            }
        }
        return $result;
    }

    /**
     * @param CustomOptions $subject
     * @param array $result
     * @param array $linkFieldIds
     * @param int $storeId
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function afterLoadProductOptions(CustomOptions $subject, array $result, array $linkFieldIds, int $storeId): array
    {
        foreach ($result as $productOptionsId  => $productOptionValue) {
            $wgtCommonExtraInformation = $productOptionValue['wgt_common_extra_information'] ?? null;
            $wgtCommonImage = $productOptionValue['wgt_common_image'] ?? null;
            if ($wgtCommonExtraInformation) {
                $result[$productOptionsId]['wgt_common_extra_information'] = $this->helperData->getExtraInformationCustomOptions($wgtCommonExtraInformation);
            }
            if ($wgtCommonImage) {
                $result[$productOptionsId]['wgt_common_image'] = $this->helperData->getWgtMediaUrl($wgtCommonImage);
            }
            if ($wgtCommonExtraInformation == 0) {
                $result[$productOptionsId]['wgt_common_extra_information'] = null;
            }
        }

        return $result;
    }


}
