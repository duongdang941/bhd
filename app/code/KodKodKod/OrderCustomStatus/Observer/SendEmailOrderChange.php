<?php

namespace KodKodKod\OrderCustomStatus\Observer;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\Area;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\DataObject;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\MailException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Framework\Translate\Inline\StateInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;

class SendEmailOrderChange implements ObserverInterface
{
    const XML_PATH_CHANGE_ORDER_EMAIL_TEMP = 'sales_email/order_change/template';
    const XML_PATH_CHANGE_ORDER_ENABLE = 'sales_email/order_change/enabled';

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;
    /**
     * @var TransportBuilder
     */
    private $transportBuilder;
    /**
     * @var StateInterface
     */
    private $inlineTranslation;
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @param ScopeConfigInterface $scopeConfig
     * @param TransportBuilder $transportBuilder
     * @param StateInterface $inlineTranslation
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        TransportBuilder $transportBuilder,
        StateInterface $inlineTranslation,
        StoreManagerInterface $storeManager
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->transportBuilder = $transportBuilder;
        $this->inlineTranslation = $inlineTranslation;
        $this->storeManager = $storeManager;
    }
    public function execute(Observer $observer)
    {
        if($this->getConfig(self::XML_PATH_CHANGE_ORDER_ENABLE)) {
            $order = $observer->getOrder();
            if ($order->getOrigData('status') != $order->getStatus() && $order->getStatus() != 'pending') {
                $this->sendEmail($order);
            }
        }
    }

    /**
     * @param $data
     */
    private function sendEmail($order)
    {
        $email = $order->getCustomerEmail();
        try {
            $this->send($email, $order);
        } catch (MailException $e) {
            throw new MailException();
        } catch (NoSuchEntityException $e) {
            throw new NoSuchEntityException();
        } catch (LocalizedException $e) {
            throw new LocalizedException();
        }
    }


    /**
     * @param $email
     * @param $order
     * @throws LocalizedException
     * @throws MailException
     * @throws NoSuchEntityException
     */
    public function send($email, $order)
    {
        $this->inlineTranslation->suspend();
        try {
            $data = [
                'order' => $order,
                'store' => $order->getStore(),
                'order_data' => [
                    'customer_name' => $order->getCustomerName(),
                    'customer_email' => $order->getCustomerEmail()
                ]
            ];
            $transportObject = new DataObject($data);

            $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
            $admin = [
                'name' => $this->scopeConfig->getValue('trans_email/ident_support/name', $storeScope),
                'email' => $this->scopeConfig->getValue('trans_email/ident_support/email', $storeScope),
            ];
            $transport = $this->transportBuilder
                ->setTemplateIdentifier($this->getConfig('sales_email/order_change/template'))
                ->setTemplateOptions(
                    [
                        'area' => Area::AREA_FRONTEND,
                        'store' => $this->storeManager->getStore()->getId()
                    ]
                )
                ->setTemplateVars($transportObject->getData())
                ->setFrom($admin)
                ->addTo($email)
                ->getTransport();

            $transport->sendMessage();
        } finally {
            $this->inlineTranslation->resume();
        }
    }
    /**
     * @param $path
     * @return mixed
     */
    public function getConfig($path)
    {
        return $this->scopeConfig->getValue(
            $path,
            ScopeInterface::SCOPE_STORE
        );
    }
}
