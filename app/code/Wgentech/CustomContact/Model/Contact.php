<?php

namespace Wgentech\CustomContact\Model;

use Magento\Framework\Model\AbstractModel;
use Wgentech\CustomContact\Model\ResourceModel\Contact as ResourceModel;

class Contact extends AbstractModel
{
    /**
     * @var string
     */
    protected $_eventPrefix = 'wgentech_contact_model';

    /**
     * @inheritdoc
     */
    protected function _construct()
    {
        $this->_init(ResourceModel::class);
    }
}
