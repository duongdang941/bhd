<?php

namespace KodKodKod\CustomProductOption\Model;

use KodKodKod\CustomProductOption\Model\ResourceModel\ExtraInformationCustomOptions as ResourceModel;
use Magento\Framework\Model\AbstractModel;

class ExtraInformationCustomOptions extends AbstractModel
{
    /**
     * @var string
     */
    protected $_eventPrefix = 'wgt_extra_information_custom_options_model';

    /**
     * @inheritdoc
     */
    protected function _construct()
    {
        $this->_init(ResourceModel::class);
    }
}
