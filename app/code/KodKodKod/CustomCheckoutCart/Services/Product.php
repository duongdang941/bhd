<?php
/**
 *
 * @copyright Copyright © 2021 Wgentech. All rights reserved.
 * @author    linh.pv68@gmail.com
 */

namespace KodKodKod\CustomCheckoutCart\Services;


/**
 * Class Product
 * KodKodKod\CustomCheckoutCart\Services
 */
class Product
{
    /**
     * @var null
     */
    private $product = null;

    /**
     * @return null
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param $product
     * @return mixed
     */
    public function setProduct($product)
    {
        return $this->product = $product;
    }

}
