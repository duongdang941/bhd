<?php

declare(strict_types=1);

namespace KodKodKod\Core\Setup\Patch\Data;

use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;
use Magento\Eav\Model\ResourceModel\Entity\Attribute;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;


/**
 * Class UpdateProduct
 *
 * @author    Agence KodKodKod
 * @copyright 2021 KodKodKod
 * @link      https://kodkodkod.studio
 */
class UpdateAttributeCategory implements DataPatchInterface
{

	/**
	 * @var EavSetup
	 */
	protected EavSetupFactory $eavSetupFactory;
	/**
	 * @var Attribute
	 */
	protected Attribute $eavAttribute;

	/**
	 * @var ModuleDataSetupInterface
	 */
	private $moduleDataSetup;


	/**
	 * UpdateAddressLabelRequired constructor.
	 * @param EavSetup $eavSetupFactory
	 * @param Attribute $eavAttribute
	 */
	public function __construct(
		ModuleDataSetupInterface $moduleDataSetup,
		EavSetupFactory $eavSetupFactory,
		Attribute $eavAttribute
	) {
		$this->eavSetupFactory = $eavSetupFactory;
		$this->eavAttribute = $eavAttribute;
		$this->moduleDataSetup = $moduleDataSetup;
	}

	/**
	 * update required value true to false for address_label attribute
	 *
	 * @return void
	 */
	public function apply(): void
	{
		$this->moduleDataSetup->getConnection()->startSetup();
		/** @var EavSetupFactory $eavSetup */
		$eavSetup = $this->eavSetupFactory->create(['setup' => $this->moduleDataSetup]);

		$eavSetup->addAttribute(
			\Magento\Catalog\Model\Category::ENTITY,
			'category_short_description',
			[
				'type' => 'text',
				'label' => 'Short Description',
				'input' => 'textarea',
				'required' => false,
				'sort_order' => 4,
				'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
				'wysiwyg_enabled' => true,
				'is_html_allowed_on_front' => true,
				'group' => 'General Information',
			]
		);

		$this->moduleDataSetup->getConnection()->endSetup();
	}

	/**
	 * {@inheritdoc}
	 *
	 * @return string[]
	 */
	public static function getDependencies(): array
	{
		return [];
	}

	/**
	 * {@inheritdoc}
	 *
	 * @return string[]
	 */
	public function getAliases(): array
	{
		return [];
	}
}
