<?php

namespace Wgentech\CustomContact\Api;

interface ContactUsManagementInterface
{
    /**
     * Contact us form.
     *
     * @param mixed $contactForm
     *
     * @return \Wgentech\CustomContact\Api\Data\ContactUsInterface
     */
    public function submitForm($contactForm);
}
