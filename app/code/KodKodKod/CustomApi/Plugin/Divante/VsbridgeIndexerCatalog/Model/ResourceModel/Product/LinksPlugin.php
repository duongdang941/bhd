<?php
/**
 *
 * Copyright © 2022 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace KodKodKod\CustomApi\Plugin\Divante\VsbridgeIndexerCatalog\Model\ResourceModel\Product;


use Divante\VsbridgeIndexerCatalog\Model\ResourceModel\Product\Links;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use KodKodKod\CustomProductOption\Helper\Data;

/**
 * KodKodKod\CustomApi\Plugin\Divante\VsbridgeIndexerCatalog\Model\ResourceModel\Product
 */
class LinksPlugin
{
    /**
     * @var CollectionFactory
     */
    private CollectionFactory $collectionFactory;
    /**
     * @var Data
     */
    private Data $imageHelper;

    /**
     * @param CollectionFactory $collectionFactory
     * @param Data $imageHelper
     */
    public function __construct(
        CollectionFactory $collectionFactory,
        Data              $imageHelper
    ){
        $this->collectionFactory = $collectionFactory;
        $this->imageHelper = $imageHelper;
    }

    /**
     * @param Links $subject
     * @param array $result
     * @param array $product
     * @return array
     */
    public function afterGetLinkedProduct(Links $subject, array $result, array $product)
    {
        if (empty($result)) {
            return $result;
        }
        $linkProductList = $result;
        $linkedProductSkuArray = array_column($linkProductList, 'linked_product_sku');

        if (!empty($linkedProductSkuArray)) {
            $productCollection = $this->collectionFactory->create()
                ->addFieldToSelect('sku')
                ->addFieldToFilter('sku', array('in', $linkedProductSkuArray))
                ->addAttributeToSelect('price');

            $linkProductList = $this->updateLinkProductListArray($productCollection, $linkProductList);
        }


        return $linkProductList;
    }

    /**
     * @param $productCollection
     * @param $linkProductList
     * @return mixed
     */
    private function updateLinkProductListArray($productCollection, $linkProductList)
    {
        $linkedProductSkuArray = array_column($linkProductList, 'linked_product_sku');
        foreach ($productCollection as $product) {
            $productSku = $product->getSku();
            if (!in_array($productSku, $linkedProductSkuArray)) {
                continue;
            }

            foreach ($linkProductList as $key => $productLink) {
                $linkedProductSku = $productLink['linked_product_sku'] ?? null;
                if ($linkedProductSku != $product->getSku()) {
                    continue;
                }
                $linkProductList[$key]['extension_attributes'] = [
                    'price' => (float)$product->getData('price'),
                    'image' => $this->imageHelper->getProductImageUrl($product)
                ];
            }
        }

        return $linkProductList;
    }
}
