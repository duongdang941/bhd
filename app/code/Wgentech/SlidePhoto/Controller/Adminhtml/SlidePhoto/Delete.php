<?php

namespace Wgentech\SlidePhoto\Controller\Adminhtml\SlidePhoto;

use Wgentech\SlidePhoto\Controller\Adminhtml\SlidePhoto;
use Wgentech\SlidePhoto\Model\SlidePhotoFactory;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Registry;

class Delete extends SlidePhoto
{
    const ADMIN_RESOURCE = 'Wgentech_SlidePhoto::delete';

    /**
     * @var SlidePhotoFactory
     */
    protected $slidePhotoFactory;

    public function __construct(
        Context           $context,
        Registry          $coreRegistry,
        SlidePhotoFactory $slidePhotoFactory
    )
    {
        $this->slidePhotoFactory = $slidePhotoFactory;
        parent::__construct($context, $coreRegistry);
    }

    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();

        $id = $this->getRequest()->getParam('id');
        if ($id) {
            try {
                $model = $this->slidePhotoFactory->create();
                $model->load($id);
                $model->delete();

                $this->messageManager->addSuccessMessage(__('You deleted the Slide Photo.'));

                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                // display error message
                $this->messageManager->addErrorMessage($e->getMessage());
                // go back to edit form
                return $resultRedirect->setPath('*/*/edit', ['id' => $id]);
            }
        }
        // display error message
        $this->messageManager->addErrorMessage(__('We can\'t find a slide photo to delete.'));
        // go to grid
        return $resultRedirect->setPath('*/*/');
    }

}