<?php

namespace KodKodKod\CustomEtransactions\Observer;

use Magento\Checkout\Model\Session;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Sales\Model\OrderFactory;

/**
 * PreDispatchRedirect class
 */
class PreDispatchRedirect implements ObserverInterface
{
    /**
     * @var Session
     */
    private $checkoutSession;
    /**
     * @var RequestInterface
     */
    private $_requestInterface;
    /**
     * @var ResourceConnection
     */
    private $_resourceConnection;
    /**
     * @var OrderFactory
     */
    private $orderFactory;

    public function __construct(
        Session            $checkoutSession,
        RequestInterface   $request,
        ResourceConnection $connection,
        OrderFactory       $orderFactory
    )
    {
        $this->checkoutSession = $checkoutSession;
        $this->_requestInterface = $request;
        $this->_resourceConnection = $connection;
        $this->orderFactory = $orderFactory;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        $orderId = null;
        $cartId = $this->_requestInterface->getParam('cart_id');
        $cartMaskedId = $this->_requestInterface->getParam('cart_mask_id');
        $connection = $this->_resourceConnection->getConnection();
        if (!$cartId) {
            $quoteMaskTable = $this->_resourceConnection->getTableName('quote_id_mask');
            $selector = $connection->select()
                ->from($quoteMaskTable, ['quote_id'])
                ->where('masked_id like (?)', $cartMaskedId);
            $result = $connection->fetchOne($selector);
            if ($result) {
                $cartId = $result;
            }
        }
        if ($cartId) {
            $saleOrderTable = $this->_resourceConnection->getTableName('sales_order');
            $selector = $connection->select()
                ->from($saleOrderTable, ['entity_id'])
                ->where('quote_id like (?)', $cartId)
                ->order("entity_id desc");
            $result = $connection->fetchOne($selector);
            $orderId = $result;
        }
        if ($orderId) {
            $order = $this->orderFactory->create()->load($orderId);
            if ($order) {
                $this->checkoutSession->setLastQuoteId($cartId);
                $this->checkoutSession->setLastSuccessQuoteId($cartId);
                $this->checkoutSession->setLastOrderId($order->getId());
                $this->checkoutSession->setLastRealOrderId($order->getIncrementId());
                $this->checkoutSession->setLastOrderStatus($order->getStatus());
            }
        }
    }
}
