<?php

namespace KodKodKod\CustomProductOption\Ui\DataProvider\Product\Form\Modifier;

use KodKodKod\CustomProductOption\Model\ResourceModel\ExtraInformationCustomOptions\Collection as ExtraInformationCustomOptionsCollection;
use Magento\Catalog\Model\Locator\LocatorInterface;
use Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\AbstractModifier;
use Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\CustomOptions as CustomOptionsModifier;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Stdlib\ArrayManager;
use Magento\Framework\UrlInterface;
use Magento\Ui\Component\Container;
use Magento\Ui\Component\Form\Element\Checkbox;
use Magento\Ui\Component\Form\Element\DataType\Text;
use Magento\Ui\Component\Form\Field;

class CustomOptions extends AbstractModifier
{
    const WGT_IMAGE_PATH = 'wgt_custom_option/images/';
    const OPTION_NONE = [
        'label' => 'None',
        'value' => "0"
    ];
    /**
     * @var array
     */
    protected $meta = [];

    /**
     * @var UrlInterface
     */
    private $urlBuilder;

    /**
     * @var LocatorInterface
     */
    private $locator;

    private DataPersistorInterface $dataPersistor;

    private ArrayManager $arrayManager;

    private \KodKodKod\CustomProductOption\Helper\Data $helperData;

    private \KodKodKod\CustomProductOption\Model\ResourceModel\ExtraInformationCustomOptions\CollectionFactory $collectionFactory;

    private ExtraInformationCustomOptionsCollection $extraInformationCustomOptionsCollection;
    private \KodKodKod\CustomProductOption\Model\Config\Source\TemplateIdOption $templateOption;

    /**
     * CustomOptions constructor.
     *
     * @param UrlInterface $urlBuilder
     * @param LocatorInterface $locator
     * @param ArrayManager $arrayManager
     * @param DataPersistorInterface $dataPersistor
     * @param \KodKodKod\CustomProductOption\Helper\Data $helperData
     */
    public function __construct(
        UrlInterface                                                                                       $urlBuilder,
        LocatorInterface                                                                                   $locator,
        ArrayManager                                                                                       $arrayManager,
        DataPersistorInterface                                                                             $dataPersistor,
        \KodKodKod\CustomProductOption\Model\Config\Source\TemplateIdOption                                $templateOption,
        \KodKodKod\CustomProductOption\Helper\Data                                                         $helperData,
        \KodKodKod\CustomProductOption\Model\ResourceModel\ExtraInformationCustomOptions\CollectionFactory $collectionFactory
    ) {
        $this->urlBuilder = $urlBuilder;
        $this->locator = $locator;
        $this->arrayManager = $arrayManager;
        $this->dataPersistor = $dataPersistor;
        $this->helperData = $helperData;
        $this->collectionFactory = $collectionFactory;
        $this->templateOption = $templateOption;
        $this->extraInformationCustomOptionsCollection = $this->collectionFactory->create();
    }

    /**
     * @param array $data
     * @return array
     */
    public function modifyData(array $data)
    {
        if (!$this->locator->getProduct()->getId() && $this->dataPersistor->get('catalog_product')) {
            return $this->resolvePersistentData($data);
        }

        $productId = $this->locator->getProduct()->getId();

        $options = $data[$productId][self::DATA_SOURCE_DEFAULT][CustomOptionsModifier::GRID_OPTIONS_NAME] ?? [];

        foreach ($options as $key => $item) {
            $wgtCommonExtraInformationId = $item['wgt_common_extra_information'] ?? null;
            $options[$key]['wgt_common_extra_information'] = $this->getExtraInformationValue($wgtCommonExtraInformationId, $productId);
            if (isset($item['wgt_common_image']) && $item['wgt_common_image'] != null) {
                $options[$key]['wgt_common_image'] =
                    [
                        0 => [
                            'file' => $item['wgt_common_image'],
                            'name' => $item['wgt_common_image'],
                            'url' => $this->helperData->getWgtMediaUrl($item['wgt_common_image'])
                        ]
                    ];
            }

            if (isset($item['values'])) {
                foreach ($item['values'] as $index => $value) {
                    $wgtExtraInformationId = $value['wgt_extra_information'] ?? null;
                    $options[$key]['values'][$index]['wgt_extra_information'] = $this->getExtraInformationValue($wgtExtraInformationId, $productId);
                    if (!isset($value['wgt_value_dynamic_image']) || $value['wgt_value_dynamic_image'] == null) {
                        continue;
                    }
                    if (isset($value['wgt_value_dynamic_image'])) {
                        $options[$key]['values'][$index]['wgt_value_dynamic_image'] =
                            [
                                0 => [
                                    'file' => $value['wgt_value_dynamic_image'],
                                    'name' => $value['wgt_value_dynamic_image'],
                                    'url' => $this->helperData->getWgtMediaUrl($value['wgt_value_dynamic_image'])
                                ]
                            ];
                    }
                }
            }
        }

        $data[$productId][self::DATA_SOURCE_DEFAULT][CustomOptionsModifier::GRID_OPTIONS_NAME] = $options;

        return $data;
    }

    private function getExtraInformationValue($wgtExtraInformationId, $productId) {
        $ExtraAdditionalFieldOptionIds = $this->extraInformationCustomOptionsCollection->addFieldToFilter('product_id', $productId)->getAllIds();

        if (in_array($wgtExtraInformationId, $ExtraAdditionalFieldOptionIds)) {
            return $wgtExtraInformationId;
        }

        return null;
    }

    /**
     * Resolve data persistence
     *
     * @param array $data
     * @return array
     */
    private function resolvePersistentData(array $data)
    {
        $persistentData = (array)$this->dataPersistor->get('catalog_product');
        $this->dataPersistor->clear('catalog_product');
        $productId = $this->locator->getProduct()->getId();

        if (empty($data[$productId][self::DATA_SOURCE_DEFAULT])) {
            $data[$productId][self::DATA_SOURCE_DEFAULT] = [];
        }

        $data[$productId] = array_replace_recursive(
            $data[$productId][self::DATA_SOURCE_DEFAULT],
            $persistentData
        );

        return $data;
    }

    /**
     * @param array $meta
     * @return array
     */
    public function modifyMeta(array $meta)
    {
        $this->meta = $meta;

        $this->addCustomOptionsFields();

        return $this->meta;
    }

    /**
     *
     */
    protected function addCustomOptionsFields()
    {
        $groupCustomOptionsName = CustomOptionsModifier::GROUP_CUSTOM_OPTIONS_NAME;
        $optionContainerName = CustomOptionsModifier::CONTAINER_OPTION;
        $commonOptionContainerName = CustomOptionsModifier::CONTAINER_COMMON_NAME;

        if (!isset($this->meta[$groupCustomOptionsName])) {
            return;
        }

        // disable auto open of collapsible custom option tab.
        $this->meta[$groupCustomOptionsName]['children']['options']['children']['record']['children'][$optionContainerName]
        ['arguments']['data']['config']['opened'] = false;
        // Add fields to the option common
        $this->meta[$groupCustomOptionsName]['children']['options']['children']['record']['children']
        [$optionContainerName]['children'][$commonOptionContainerName]['children'] = array_replace_recursive(
            $this->meta[$groupCustomOptionsName]['children']['options']['children']['record']['children']
            [$optionContainerName]['children'][$commonOptionContainerName]['children'],
            $this->getOptionFieldsConfig()
        );

        // Add fields to the values
        $this->meta[$groupCustomOptionsName]['children']['options']['children']['record']['children']
        [$optionContainerName]['children']['values']['children']['record']['children'] = array_replace_recursive(
            $this->meta[$groupCustomOptionsName]['children']['options']['children']['record']['children']
            [$optionContainerName]['children']['values']['children']['record']['children'],
            $this->getValueFieldsDynamicConfig()
        );
    }

    /**
     * @return array
     */
    private function getOptionFieldsConfig()
    {
        $fields['wgt_formulated_option'] = $this->getTextField(__('Formulated Option'), 'wgt_formulated_option', 85);
        $fields['wgt_answer_option'] = $this->getTextField(__('Answer Option'), 'wgt_answer_option', 60);
        $fields['wgt_template_id'] = $this->getTemplateIdFields(__('Template ID'),'wgt_template_id', 200);
        $fields['wgt_common_question_code'] = $this->getTextField(__('Question Code Common'), 'wgt_common_question_code', 100);
        $fields['wgt_common_depend_on'] = $this->getTextField(__('Depend On Common'), 'wgt_common_depend_on', 150);
        $fields['wgt_common_extra_information'] = $this->getExtraInformationFields(__('Extra Information Common'),'wgt_common_extra_information', 200);
        $fields['wgt_common_image'] = $this->getImageFieldConfig(210);
        $fields['sort_order'] = $this->getTextField(__('Sort Order'), 'sort_order', '80') ;
        $fields['wgt_option_required'] = $this->getCheckboxField(__('Option Required'), 'wgt_option_required', 75) ;
        return $fields;
    }

    /**
     * @return array
     */
    protected function getValueFieldsDynamicConfig()
    {
        $fields['option_code'] = $this->getTextField(__('Option Code'), 'option_code', 41);
        $fields['depend_on'] = $this->getTextField(__('Depend On'), 'depend_on', 45);
        $fields['wgt_value_dynamic_image'] = $this->getImageFieldOptionConfig(55);
        $fields['wgt_extra_information'] = $this->getExtraInformationFields(__('Extra Information'),'wgt_extra_information', 50);

        return $fields;
    }

    /**
     * @param $label
     * @param $dataScope
     * @param $sortOrder
     * @return \array[][][]
     */
    protected function getTextField($label, $dataScope, $sortOrder)
    {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'label' => $label,
                        'componentType' => \Magento\Ui\Component\Form\Field::NAME,
                        'formElement' => \Magento\Ui\Component\Form\Element\Input::NAME,
                        'dataScope' => $dataScope,
                        'dataType' => \Magento\Ui\Component\Form\Element\DataType\Text::NAME,
                        'sortOrder' => $sortOrder,
                    ],
                ],
            ],
        ];
    }
    protected function getTemplateIdFields($label, $dataScope, $sortOrder){

        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'label' => $label,
                        'componentType' => Field::NAME,
                        'component' => 'Magento_Ui/js/form/element/ui-select',
                        'formElement' => \Magento\Ui\Component\Form\Element\Select::NAME,
                        'elementTmpl' => 'ui/grid/filters/elements/ui-select',
                        'disableLabel' => true,
                        'filterOptions' => true,
                        'multiple' => false,
                        'options' => $this->templateOption->toOptionArray(),
                        'dataScope' => $dataScope,
                        'sortOrder' => $sortOrder,
                    ],
                ],
            ]
        ];
    }
    protected function getExtraInformationFields($label, $dataScope, $sortOrder){
        $productId = $this->locator->getProduct()->getId();
        $data = [
            'arguments' => [
                'data' => [
                    'config' => [
                        'label' => $label,
                        'componentType' => Field::NAME,
                        'component' => 'Magento_Ui/js/form/element/ui-select',
                        'formElement' => \Magento\Ui\Component\Form\Element\Select::NAME,
                        'elementTmpl' => 'ui/grid/filters/elements/ui-select',
                        'disableLabel' => true,
                        'filterOptions' => true,
                        'multiple' => false,
                        'options' => $this->extraInformationCustomOptionsCollection->addFieldToFilter('product_id', $productId)->toOptionArray(),
                        'dataScope' => $dataScope,
                        'sortOrder' => $sortOrder,
                    ],
                ],
            ]
        ];
        if(in_array($dataScope, ['wgt_common_extra_information', 'wgt_extra_information'])) {
            array_unshift($data['arguments']['data']['config']['options'], self::OPTION_NONE);
        }
        return $data;
    }


    /**
     * @param $sortOrder
     * @return \array[][][]
     */
    protected function getImageFieldOptionConfig($sortOrder)
    {
        $sampleContainer['arguments']['data']['config'] = [
            'componentType' => Container::NAME,
            'formElement' => Container::NAME,
            'component' => 'Magento_Ui/js/form/components/group',
            'label' => __('Image'),
            'showLabel' => false,
            'dataScope' => '',
            'sortOrder' => $sortOrder,
        ];

        $sampleUploader['arguments']['data']['config'] = [
            'formElement' => 'fileUploader',
            'componentType' => 'fileUploader',
            'component' => 'Magento_Ui/js/form/element/image-uploader',
            'elementTmpl' => 'KodKodKod_CustomProductOption/file-uploader',
            'previewTmpl' => 'Magento_Catalog/image-preview',
            'fileInputName' => 'wgt_value_dynamic_image',
            'uploaderConfig' => [
                'url' => $this->urlBuilder->getUrl(
                    'wgt_custom_option/upload/image',
                    ['type' => 'wgt_value_dynamic_image', '_secure' => true]
                ),
            ],
            'dataScope' => 'wgt_value_dynamic_image'
        ];

        return $this->arrayManager->set(
            'children',
            $sampleContainer,
            [
                'wgt_value_dynamic_image' => $sampleUploader
            ]
        );
    }

    /**
     * @param $sortOrder
     * @return \array[][][]
     */
    protected function getImageFieldConfig($sortOrder)
    {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'label' => __('Image Uploader'),
                        'componentType' => 'imageUploader',
                        'formElement' => 'imageUploader',
                        'component' => 'Magento_Ui/js/form/element/image-uploader',
                        'elementTmpl' => 'KodKodKod_CustomProductOption/file-uploader',
                        'previewTmpl' => 'Magento_Catalog/image-preview',
                        'dataScope' => 'wgt_common_image',
                        'fileInputName' => 'wgt_common_image',
                        'uploaderConfig' => [
                            'url' => $this->urlBuilder->getUrl(
                                'wgt_custom_option/upload/image',
                                ['type' => 'wgt_common_image', '_secure' => true]
                            ),
                        ],
                        'sortOrder' => $sortOrder,
                    ]
                ],
            ]
        ];
    }

    /**
     * @param $label
     * @param $dataScope
     * @param $sortOrder
     * @return \array[][][]
     */
    protected function getCheckboxField($label, $dataScope, $sortOrder)
    {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'label' => $label,
                        'componentType' => Field::NAME,
                        'formElement' => Checkbox::NAME,
                        'dataScope' => $dataScope,
                        'dataType' => Text::NAME,
                        'sortOrder' => $sortOrder,
                        'value' => '1',
                        'valueMap' => [
                            'true' => '1',
                            'false' => '0'
                        ],
                    ],
                ],
            ],
        ];
    }

}
