<?php
/**
 *
 * Copyright © 2022 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace KodKodKod\CustomApi\Plugin\Magento\Model;


use Magento\Catalog\Model\ResourceModel\Product\Collection;

class AddPriceToCollection
{
    /**
     * Add price information to collection.
     *
     * @param Collection $productCollection
     * @param bool $printQuery
     * @param bool $logQuery
     * @return array
     */
    public function beforeLoad(Collection $productCollection, $printQuery = false, $logQuery = false)
    {
        $productCollection->addAttributeToSelect('price')->addAttributeToSelect('image');
        return [$printQuery, $logQuery];
    }

}
