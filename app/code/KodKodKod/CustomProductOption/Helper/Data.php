<?php

namespace KodKodKod\CustomProductOption\Helper;

use KodKodKod\CustomProductOption\Model\ExtraInformationCustomOptionsFactory;
use KodKodKod\CustomProductOption\Model\ResourceModel\ExtraInformationCustomOptions;
use Magento\Catalog\Model\Product\Image\UrlBuilder;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Store\Model\StoreManagerInterface;

class Data extends AbstractHelper
{
    const WGT_IMAGE_PATH = 'wgt_custom_option/images/';

    /**
     * @var StoreManagerInterface
     */
    private StoreManagerInterface $storeManager;

    /**
     * @var SerializerInterface
     */
    private SerializerInterface $serialize;

    /**
     * @var ExtraInformationCustomOptionsFactory
     */
    private ExtraInformationCustomOptionsFactory $informationCustomOptionsFactory;

    /**
     * @var ExtraInformationCustomOptions
     */
    private ExtraInformationCustomOptions $extraInformationCustomOptionsResourceModel;

    /**
     * @var UrlBuilder
     */
    private UrlBuilder $imageUrlBuilder;

    /**
     * Data constructor.
     *
     * @param Context $context
     * @param StoreManagerInterface $storeManager
     * @param ExtraInformationCustomOptionsFactory $informationCustomOptionsFactory
     * @param ExtraInformationCustomOptions $extraInformationCustomOptionsResourceModel
     * @param SerializerInterface $serialize
     */
    public function __construct(
        Context                              $context,
        StoreManagerInterface                $storeManager,
        ExtraInformationCustomOptionsFactory $informationCustomOptionsFactory,
        ExtraInformationCustomOptions        $extraInformationCustomOptionsResourceModel,
        SerializerInterface                  $serialize,
        UrlBuilder $urlBuilder
    ) {
        $this->storeManager = $storeManager;
        $this->serialize = $serialize;
        $this->informationCustomOptionsFactory = $informationCustomOptionsFactory;
        $this->extraInformationCustomOptionsResourceModel = $extraInformationCustomOptionsResourceModel;
        $this->imageUrlBuilder = $urlBuilder;
        parent::__construct($context);
    }

    /**
     * @param null $path
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getWgtMediaUrl($path = null)
    {
        $mediaUrl = $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        $mediaUrl .= self::WGT_IMAGE_PATH;
        $mediaUrl .= $path;

        return $mediaUrl;
    }

    /**
     * @param $id
     * @return array|mixed|null
     */
    public function getExtraInformationCustomOptions($id)
    {
        $model = $this->informationCustomOptionsFactory->create();
        $this->extraInformationCustomOptionsResourceModel->load($model, $id);

        $extraInformation = $model->getData();
        if (empty($extraInformation)) {
            return null;
        }

        $dynamicValue = $extraInformation['dynamic_value'] ?? null;

        if ($dynamicValue != null) {
            $dynamicValue = $this->serialize->unserialize($dynamicValue);
            foreach ($dynamicValue as $key => $value) {
                unset($value['record_id']);
                unset($value['initialize']);
                $dynamicValue[$key] = $value;
            }

            $extraInformation['dynamic_value'] = $dynamicValue;
        }

        unset($extraInformation['option_id']);
        unset($extraInformation['product_id']);
        unset($extraInformation['sort_order']);

        return $this->convertExtraInformation($extraInformation);
    }

    /**
     * @param $product
     * @return null|string
     */
    public function getProductImageUrl($product): ?string
    {
        if ($product->getImage()) {
            return $this->imageUrlBuilder->getUrl($product->getImage(), 'product_page_main_image');
        }
        return null;
    }


    /**
     * @param $extraInformation
     * @return array|mixed
     */
    public function convertExtraInformation($extraInformation) {
        $dynamicValue = $extraInformation['dynamic_value'] ?? null;

        if ($dynamicValue == null) {
            return $extraInformation;
        }

        foreach ($dynamicValue as $key => $information) {
            $imageUrl = null;
            $extraOptionDynamicImage = $information['extra_option_dynamic_image'] ?? null;
            $imageName = $extraOptionDynamicImage[0]['name'] ?? null;
            if ($imageName) {
                try {
                    $imageUrl = $this->getWgtMediaUrl($imageName);
                } catch (NoSuchEntityException $e) {
                    $this->_logger->critical($e->getMessage());
                }
            }
            $extraInformation['dynamic_value'][$key]['extra_option_dynamic_image'] = $imageUrl;
        }

        return $extraInformation;
    }

}
