<?php

namespace Wgentech\SlidePhoto\Model;

use Wgentech\SlidePhoto\Api\SlidePhotoInterface;
use Wgentech\SlidePhoto\Model\ResourceModel\SlidePhoto\CollectionFactory;
use Magento\Store\Model\StoreManagerInterface;

class SlidePhotoApi implements SlidePhotoInterface
{
    const ACTIVE = 1;
    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    public function __construct(
        StoreManagerInterface $storeManager,
        CollectionFactory $collectionFactory
    )
    {
        $this->storeManager = $storeManager;
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * @return mixed|void
     */
    public function getList()
    {
        try {
            $responses = $this->collectionFactory->create()->addFieldToFilter('is_active',self::ACTIVE)->getData();
            if($responses){
                foreach ($responses as $key => $response){
                    $nameImg = $response['image'] ?? null;
                    $pathImg = $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA).'slidephoto/images/'.$nameImg;
                    $responses[$key]['pathImg'] = $pathImg;
                }
            }

        } catch (\Exception $e){
            $responses=['error' => $e->getMessage()];
        }
        return json_encode($responses);
    }
}