<?php

namespace KodKodKod\CustomProductOption\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class ExtraInformationCustomOptions extends AbstractDb
{
    /**
     * @var string
     */
    protected $_eventPrefix = 'wgt_extra_information_custom_options_resource_model';

    protected $_idFieldName = 'option_id';

    /**
     * @inheritdoc
     */
    protected function _construct()
    {
        $this->_init('wgt_extra_information_custom_options', 'option_id');
        $this->_useIsObjectNew = true;
    }
}
