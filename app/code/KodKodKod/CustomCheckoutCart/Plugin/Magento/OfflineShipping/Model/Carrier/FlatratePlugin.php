<?php

namespace KodKodKod\CustomCheckoutCart\Plugin\Magento\OfflineShipping\Model\Carrier;


use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Quote\Model\Quote\Address\RateResult\MethodFactory;
use Magento\Shipping\Model\Rate\ResultFactory;

class FlatratePlugin
{
    const COUNTRY_CODE_BELGIUM = 'BE';
    /**
     * @var ResultFactory
     */
    private $_rateResultFactory;
    /**
     * @var MethodFactory
     */
    private $_rateMethodFactory;

    public function __construct(
        ResultFactory $rateResultFactory,
        MethodFactory $rateMethodFactory

    )
    {
        $this->_rateResultFactory = $rateResultFactory;
        $this->_rateMethodFactory = $rateMethodFactory;
    }

    public function afterCollectRates(
        \Magento\OfflineShipping\Model\Carrier\Flatrate $subject,
                                                        $result,
        RateRequest                                     $request
    )
    {
        if ($request->getDestCountryId() == self::COUNTRY_CODE_BELGIUM) {
            $configPriceBelgium = $subject->getConfigData('price_custom_flat_rate');
            if (empty($configPriceBelgium) && $configPriceBelgium == 0) {

                return $result;
            }
            $resultBelgium = $this->_rateResultFactory->create();
            $method = $this->_rateMethodFactory->create();
            $method->setCarrier('flatrate');
            $method->setCarrierTitle($subject->getConfigData('title'));
            $method->setMethod('flatrate');
            $method->setMethodTitle($subject->getConfigData('name'));
            $method->setPrice($configPriceBelgium);
            $method->setCost($configPriceBelgium);
            $resultBelgium->append($method);

            return $resultBelgium;

        }

        return $result;
    }

}
