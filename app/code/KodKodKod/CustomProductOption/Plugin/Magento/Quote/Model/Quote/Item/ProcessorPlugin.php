<?php

namespace KodKodKod\CustomProductOption\Plugin\Magento\Quote\Model\Quote\Item;

use Magento\Catalog\Model\Product;
use Magento\Framework\DataObject;
use Magento\Framework\Exception\LocalizedException;
use Magento\Quote\Model\Quote\Item;
use Magento\Quote\Model\Quote\Item\Processor;

class ProcessorPlugin
{
    /**
     * @param Processor $subject
     * @param $result
     * @param Item $item
     * @param DataObject $request
     * @param Product $candidate
     * @throws LocalizedException
     */
    public function afterPrepare(
        Processor  $subject,
                   $result,
        Item       $item,
        DataObject $request,
        Product    $candidate
    )
    {
        $product = $candidate;
        $customOptionPrice = 0.00;
        $optionIds = $product->getCustomOption('option_ids');
        if ($optionIds) {
            $basePrice = $product->getPriceModel()->getBasePrice($product, null);
            foreach (explode(',', $optionIds->getValue()) as $optionId) {
                if ($option = $product->getOptionById($optionId)) {
                    $confItemOption = $product->getCustomOption('option_' . $option->getId());

                    $group = $option->groupFactory($option->getType())
                        ->setOption($option)
                        ->setConfigurationItemOption($confItemOption);
                    $customOptionPrice += $group->getOptionPrice($confItemOption->getValue(), $basePrice);
                }
            }
        }
        if ($customOptionPrice > 0) {
            $item->addOption([
                'code' => 'wgt_custom_option_price',
                'value' => $customOptionPrice,
            ]);
        }
    }
}
