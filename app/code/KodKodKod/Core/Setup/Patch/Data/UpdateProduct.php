<?php

declare(strict_types=1);

namespace KodKodKod\Core\Setup\Patch\Data;

use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;
use Magento\Eav\Model\ResourceModel\Entity\Attribute;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;

/**
 * Class UpdateProduct
 *
 * @author    Agence KodKodKod
 * @copyright 2021 KodKodKod
 * @link      https://kodkodkod.studio
 */
class UpdateProduct implements DataPatchInterface
{
	/**
	 * @var EavSetup
	 */
	protected EavSetupFactory $eavSetupFactory;
	/**
	 * @var Attribute
	 */
	protected Attribute $eavAttribute;

	/**
	 * @var ModuleDataSetupInterface
	 */
	private $moduleDataSetup;


	/**
	 * UpdateAddressLabelRequired constructor.
	 * @param EavSetup $eavSetupFactory
	 * @param Attribute $eavAttribute
	 */
	public function __construct(
		ModuleDataSetupInterface $moduleDataSetup,
		EavSetupFactory $eavSetupFactory,
		Attribute $eavAttribute
	) {
		$this->eavSetupFactory = $eavSetupFactory;
		$this->eavAttribute = $eavAttribute;
		$this->moduleDataSetup = $moduleDataSetup;
	}

	/**
	 * update required value true to false for address_label attribute
	 *
	 * @return void
	 */
	public function apply(): void
	{

		$this->moduleDataSetup->getConnection()->startSetup();
		/** @var EavSetupFactory $eavSetup */
		$eavSetup = $this->eavSetupFactory->create(['setup' => $this->moduleDataSetup]);

		$eavSetup->addAttribute(
			\Magento\Catalog\Model\Product::ENTITY,
			'feature_1_value',
			[
				'group' => 'General',
				'type' => 'varchar',
				'backend' => '',
				'frontend' => '',
				'label' => 'Feature 1 Value',
				'input' => 'text',
				'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
				'visible' => true,
				'required' => false,
				'user_defined' => false,
				'searchable' => false,
				'filterable' => false,
				'comparable' => false,
				'visible_on_front' => false,
				'used_in_product_listing' => false,
				'unique' => false,
				'apply_to' => 'simple,configurable,virtual,bundle,downloadable'
			]
		);

		$eavSetup->addAttribute(
			\Magento\Catalog\Model\Product::ENTITY,
			'feature_1_value',
			[
				'group' => 'General',
				'type' => 'varchar',
				'backend' => '',
				'frontend' => '',
				'label' => 'Feature 1 Value',
				'input' => 'text',
				'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
				'visible' => true,
				'required' => false,
				'user_defined' => false,
				'searchable' => false,
				'filterable' => false,
				'comparable' => false,
				'visible_on_front' => false,
				'used_in_product_listing' => false,
				'unique' => false,
				'apply_to' => 'simple,configurable,virtual,bundle,downloadable'
			]
		);

		$eavSetup->addAttribute(
			\Magento\Catalog\Model\Product::ENTITY,
			'feature_2_value',
			[
				'group' => 'General',
				'type' => 'varchar',
				'backend' => '',
				'frontend' => '',
				'label' => 'Feature 2 Value',
				'input' => 'text',
				'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
				'visible' => true,
				'required' => false,
				'user_defined' => false,
				'searchable' => false,
				'filterable' => false,
				'comparable' => false,
				'visible_on_front' => false,
				'used_in_product_listing' => false,
				'unique' => false,
				'apply_to' => 'simple,configurable,virtual,bundle,downloadable'
			]
		);

		$eavSetup->addAttribute(
			\Magento\Catalog\Model\Product::ENTITY,
			'feature_3_value',
			[
				'group' => 'General',
				'type' => 'varchar',
				'backend' => '',
				'frontend' => '',
				'label' => 'Feature 3 Value',
				'input' => 'text',
				'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
				'visible' => true,
				'required' => false,
				'user_defined' => false,
				'searchable' => false,
				'filterable' => false,
				'comparable' => false,
				'visible_on_front' => false,
				'used_in_product_listing' => false,
				'unique' => false,
				'apply_to' => 'simple,configurable,virtual,bundle,downloadable'
			]
		);

		$eavSetup->addAttribute(
			\Magento\Catalog\Model\Product::ENTITY,
			'feature_4_value',
			[
				'group' => 'General',
				'type' => 'varchar',
				'backend' => '',
				'frontend' => '',
				'label' => 'Feature 4 Value',
				'input' => 'text',
				'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
				'visible' => true,
				'required' => false,
				'user_defined' => false,
				'searchable' => false,
				'filterable' => false,
				'comparable' => false,
				'visible_on_front' => false,
				'used_in_product_listing' => false,
				'unique' => false,
				'apply_to' => 'simple,configurable,virtual,bundle,downloadable'
			]
		);

		$eavSetup->addAttribute(
			\Magento\Catalog\Model\Product::ENTITY,
			'feature_1_text',
			[
				'group' => 'General',
				'type' => 'varchar',
				'backend' => '',
				'frontend' => '',
				'label' => 'Feature 1 Text',
				'input' => 'text',
				'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
				'visible' => true,
				'required' => false,
				'user_defined' => false,
				'searchable' => false,
				'filterable' => false,
				'comparable' => false,
				'visible_on_front' => false,
				'used_in_product_listing' => false,
				'unique' => false,
				'apply_to' => 'simple,configurable,virtual,bundle,downloadable'
			]
		);

		$eavSetup->addAttribute(
			\Magento\Catalog\Model\Product::ENTITY,
			'feature_2_text',
			[
				'group' => 'General',
				'type' => 'varchar',
				'backend' => '',
				'frontend' => '',
				'label' => 'Feature 2 Text',
				'input' => 'text',
				'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
				'visible' => true,
				'required' => false,
				'user_defined' => false,
				'searchable' => false,
				'filterable' => false,
				'comparable' => false,
				'visible_on_front' => false,
				'used_in_product_listing' => false,
				'unique' => false,
				'apply_to' => 'simple,configurable,virtual,bundle,downloadable'
			]
		);

		$eavSetup->addAttribute(
			\Magento\Catalog\Model\Product::ENTITY,
			'feature_3_text',
			[
				'group' => 'General',
				'type' => 'varchar',
				'backend' => '',
				'frontend' => '',
				'label' => 'Feature 3 Text',
				'input' => 'text',
				'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
				'visible' => true,
				'required' => false,
				'user_defined' => false,
				'searchable' => false,
				'filterable' => false,
				'comparable' => false,
				'visible_on_front' => false,
				'used_in_product_listing' => false,
				'unique' => false,
				'apply_to' => 'simple,configurable,virtual,bundle,downloadable'
			]
		);

		$eavSetup->addAttribute(
			\Magento\Catalog\Model\Product::ENTITY,
			'feature_4_text',
			[
				'group' => 'General',
				'type' => 'varchar',
				'backend' => '',
				'frontend' => '',
				'label' => 'Feature 4 Text',
				'input' => 'text',
				'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
				'visible' => true,
				'required' => false,
				'user_defined' => false,
				'searchable' => false,
				'filterable' => false,
				'comparable' => false,
				'visible_on_front' => false,
				'used_in_product_listing' => false,
				'unique' => false,
				'apply_to' => 'simple,configurable,virtual,bundle,downloadable'
			]
		);

		$this->moduleDataSetup->getConnection()->endSetup();
	}

	/**
	 * {@inheritdoc}
	 *
	 * @return string[]
	 */
	public static function getDependencies(): array
	{
		return [];
	}

	/**
	 * {@inheritdoc}
	 *
	 * @return string[]
	 */
	public function getAliases(): array
	{
		return [];
	}
}
