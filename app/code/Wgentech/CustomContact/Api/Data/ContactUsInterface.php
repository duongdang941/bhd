<?php

namespace Wgentech\CustomContact\Api\Data;

interface ContactUsInterface
{
    /**
     * @return \Wgentech\CustomContact\Api\Data\ContactUsInterface[]
     */
    public function getMessage();

    /**
     * @param string $message
     * @return $this
     */
    public function setMessage($message);
}
