<?php
/**
 *
 * Copyright © 2022 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace KodKodKod\CustomVsbridgeIndexerCatalog\Plugin\Divante\VsbridgeIndexerCatalog\ArrayConverter\Product;


use Divante\VsbridgeIndexerCore\Index\DataFilter;

/**
 * KodKodKod\CustomVsbridgeIndexerCatalog\Plugin\Divante\VsbridgeIndexerCatalog\ArrayConverter\Product
 */
class CustomOptionConverter extends \Divante\VsbridgeIndexerCatalog\ArrayConverter\Product\CustomOptionConverter
{
    /**
     * @var array
     */
    private $fieldsToDelete = [
        'default_title',
        'store_title',
        'default_price',
        'default_price_type',
        'store_price',
        'store_price_type',
        'product_id',
    ];

    /**
     * @var DataFilter
     */
    private $dataFilter;

    /**
     * CustomOptionConverter constructor.
     *
     * @param DataFilter $dataFilter
     */
    public function __construct(DataFilter $dataFilter)
    {
        $this->dataFilter = $dataFilter;
        parent::__construct($dataFilter);
    }

    /**
     * @param array $options
     * @param array $optionValues
     *
     * @return array
     */
    public function process(array $options, array $optionValues): array
    {
        $groupOption = [];

        foreach ($optionValues as $optionValue) {
            $optionId = $optionValue['option_id'];
            $optionValue = $this->prepareValue($optionValue);
            $options[$optionId]['values'][] = $optionValue;
        }

        foreach ($options as $option) {
            $productId = $option['product_id'];
            $option = $this->prepareOption($option);
            $groupOption[$productId][] = $option;
        }

        return $groupOption;
    }

    /**
     * @param array $option
     *
     * @return array
     */
    private function prepareValue(array $option): array
    {
        $option = $this->filterData($option);
        unset($option['option_id']);

        return $option;
    }

    /**
     * @param array $option
     *
     * @return array
     */
    private function filterData(array $option): array
    {
        $option = $this->dataFilter->execute($option, $this->fieldsToDelete);
        $option['sort_order'] = (int) $option['sort_order'];
        // Wgentech - Fix bug option_id is incorrect when reindex.
        if (!isset($option['option_id'])) {
            $option['option_id'] = (int) $option['sort_order'];
        }
        if (!isset($option['option_type_id'])) {
            $option['option_type_id'] = (int) $option['sort_order'];
        }
        // Wgentech - End Fix bug option_id.
        $option['price'] = (float) $option['price'];

        if (isset($option['sku']) !== true) {
            unset($option['sku']);
        }

        if (isset($option['file_extension']) !== true) {
            unset($option['file_extension']);
        }

        return $option;
    }

    /**
     * @param array $option
     *
     * @return array
     */
    private function prepareOption(array $option): array
    {
        $option['is_require'] = (boolean)$option['is_require'];
        $option = $this->filterData($option);

        if ('drop_down' === $option['type']) {
            $option['type'] = 'select';
        }

        return $option;
    }

}
