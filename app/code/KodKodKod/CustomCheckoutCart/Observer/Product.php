<?php
/**
 * Product
 *
 * @copyright Copyright © 2021 Wgentech. All rights reserved.
 * @author    linh.pv68@gmail.com
 */

namespace KodKodKod\CustomCheckoutCart\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

/**
 * Class Product
 * KodKodKod\CustomCheckoutCart\Observer
 */
class Product implements ObserverInterface
{
    /**
     * @var \KodKodKod\CustomCheckoutCart\Services\Product
     */
    private \KodKodKod\CustomCheckoutCart\Services\Product $productServices;

    /**
     * Product constructor.
     *
     * @param \KodKodKod\CustomCheckoutCart\Services\Product $productServices
     */
    public function __construct(
        \KodKodKod\CustomCheckoutCart\Services\Product $productServices
    ) {
        $this->productServices = $productServices;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        $product = $observer->getEvent()->getData('product');
        $this->productServices->setProduct($product);
    }
}
