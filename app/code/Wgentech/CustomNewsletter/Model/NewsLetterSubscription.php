<?php

namespace Wgentech\CustomNewsletter\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Newsletter\Model\Subscriber;
use Magento\Framework\Validator\EmailAddress as EmailValidator;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Newsletter\Model\SubscriberFactory;
use Magento\Customer\Model\Session;
use Magento\Customer\Model\Url;
use Magento\Customer\Api\AccountManagementInterface;
use Magento\Newsletter\Model\SubscriptionManagerInterface;

class NewsLetterSubscription implements \Wgentech\CustomNewsletter\Api\NewsLetterSubscriptionInterface
{
    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;
    /**
     * @var SubscriberFactory
     */
    protected $subscriberFactory;
    /**
     * @var EmailValidator
     */
    protected $emailValidator;
    /**
     * @var Session
     */
    protected $session;
    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;
    /**
     * @var Url
     */
    protected $customerUrl;
    /**
     * @var AccountManagementInterface
     */
    protected $customerAccountManagement;
    /**
     * @var SubscriptionManagerInterface
     */
    protected $subscriptionManager;

    public function __construct(
        StoreManagerInterface        $storeManager,
        SubscriberFactory            $subscriberFactory,
        EmailValidator               $emailValidator,
        Session                      $session,
        ScopeConfigInterface         $scopeConfig,
        Url                          $customerUrl,
        AccountManagementInterface   $customerAccountManagement,
        SubscriptionManagerInterface $subscriptionManager
    )
    {
        $this->storeManager = $storeManager;
        $this->subscriberFactory = $subscriberFactory;
        $this->emailValidator = $emailValidator;
        $this->session = $session;
        $this->scopeConfig = $scopeConfig;
        $this->customerUrl = $customerUrl;
        $this->customerAccountManagement = $customerAccountManagement;
        $this->subscriptionManager = $subscriptionManager;
    }

    /**
     * @param $customerEmail
     * @return false|mixed|string
     * @throws \Exception
     */
    public function postNewsLetter($customerEmail)
    {
        try {
        $customerEmail = trim($customerEmail);
        if (empty($customerEmail)) {
            throw new \Exception(
                __('You must specify an email address to subscribe to a newsletter.')
            );
        }
            $this->validateEmailFormat($customerEmail);
            $this->validateGuestSubscription();
            $this->validateEmailAvailable($customerEmail);

            $websiteId = (int)$this->storeManager->getStore()->getWebsiteId();
            /** @var Subscriber $subscriber */
            $subscriber = $this->subscriberFactory->create()->loadBySubscriberEmail($customerEmail, $websiteId);
            if ($subscriber->getId()
                && (int)$subscriber->getSubscriberStatus() === Subscriber::STATUS_SUBSCRIBED) {
                throw new \Exception(
                    __('This email address is already subscribed.')
                );
            }
            $storeId = (int)$this->storeManager->getStore()->getId();
            $currentCustomerId = $this->getSessionCustomerId($customerEmail);
            $subscriber = $currentCustomerId
                ? $this->subscriptionManager->subscribeCustomer($currentCustomerId, $storeId)
                : $this->subscriptionManager->subscribe($customerEmail, $storeId);
            $response[] = ['success' => true, 'message' => __('Thank you for your subscription.')];

        } catch (\Exception $e) {
            $response[] = ['success' => false, 'message' => $e->getMessage()];
        }
        return $response;
    }

    /**
     * @throws \Exception
     */
    protected function validateEmailFormat($email)
    {
        if (!$this->emailValidator->isValid($email)) {
            throw new \Exception(__('Please enter a valid email address.'));
        }
    }

    /**
     * @return void
     * @throws \Exception
     */
    protected function validateGuestSubscription()
    {
        if ($this->scopeConfig->getValue(
                Subscriber::XML_PATH_ALLOW_GUEST_SUBSCRIBE_FLAG,
                ScopeInterface::SCOPE_STORE) != 1
            && !$this->session->isLoggedIn()
        ) {
            throw new \Exception(
                __(
                    'Sorry, but the administrator denied subscription for guests. Please <a href="%1">register</a>.',
                    $this->customerUrl->getRegisterUrl()
                )
            );
        }
    }

    /**
     * @param $email
     * @return void
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Exception
     */
    protected function validateEmailAvailable($email)
    {
        $websiteId = $this->storeManager->getStore()->getWebsiteId();
        if ($this->session->isLoggedIn()
            && ($this->session->getCustomerDataObject()->getEmail() !== $email
                && !$this->customerAccountManagement->isEmailAvailable($email, $websiteId))
        ) {
            throw new \Exception(
                __('This email address is already assigned to another user.')
            );
        }
    }

    /**
     * @param $email
     * @return int|null
     */
    private function getSessionCustomerId($email)
    {
        if (!$this->session->isLoggedIn()) {
            return null;
        }
        $customer = $this->session->getCustomerDataObject();
        if ($customer->getEmail() !== $email) {
            return null;
        }
        return (int)$this->session->getId();
    }
}
