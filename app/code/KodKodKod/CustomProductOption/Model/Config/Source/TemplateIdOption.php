<?php

namespace KodKodKod\CustomProductOption\Model\Config\Source;

use Magento\Framework\Data\OptionSourceInterface;

class TemplateIdOption implements OptionSourceInterface
{
    /**
     * to option array
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options[] = [
            'value' => '',
            'label' => __('Select')
        ];

        $options[] = [
            'value' => "0",
            'label' => __('None')
        ];

        $allOptions = $this->getAllOptionArray();

        foreach ($allOptions as $optionValue => $optionValueLabel) {
            $value = $optionValue;
            $label = $optionValueLabel;
            $options[] = [
                'value' => $value,
                'label' => $label
            ];
        }

        return $options;
    }

    public function getAllOptionArray()
    {
        return [
            'top-image-select' => __('Top Image Selection'),
            'select' => __('Top Image Selection'),
            'information-select' => __('Information Selection'),
            'image-input-list' => __('Input List '),
            'image-select' => __('Image Selection'),
            'quantity-input' => __('Quantity Input'),
            'color-picker' => __('Color Picker'),
            'multiple-with-input' => __('Multiple with Input'),
            'colors-for-information-selection' => __('Colors for Information Selection')
        ];
    }
}
