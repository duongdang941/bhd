<?php

namespace KodKodKod\Catalog\Observer\Catalog;

use Magento\Framework\Event\Observer;
use Magento\Widget\Model\Template\FilterEmulate;
use Psr\Log\LoggerInterface;

class CategoryLoadAfter implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;
    /**
     * @var FilterEmulate
     */
    private FilterEmulate $filterEmulate;

    /**
     * @param LoggerInterface $logger
     * @param FilterEmulate $filterEmulate
     */
    public function __construct(
        LoggerInterface $logger,
        FilterEmulate   $filterEmulate
    )
    {
        $this->logger = $logger;
        $this->filterEmulate = $filterEmulate;
    }

    /**
     * Execute observer
     *
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        $category = $observer->getEvent()->getCategory();

        if ($category instanceof \Magento\Catalog\Model\Category) {
            $description = $category->getDescription();
            $descriptionFilter = $this->filterEmulate->filter($description);
            $category->setDescription($descriptionFilter);
        }
    }
}
