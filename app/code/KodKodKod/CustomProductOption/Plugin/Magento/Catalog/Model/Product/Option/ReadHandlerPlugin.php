<?php

namespace KodKodKod\CustomProductOption\Plugin\Magento\Catalog\Model\Product\Option;

use Magento\Catalog\Api\ProductCustomOptionRepositoryInterface;
use Magento\Catalog\Model\Product\Option\ReadHandler;
use Psr\Log\LoggerInterface;
use Magento\Widget\Model\Template\FilterEmulate;
use Magento\Catalog\Block\Product\View\Options;
use KodKodKod\CustomProductOption\Helper\OptionsProvider;
use KodKodKod\Catalog\Helper\Data;
use KodKodKod\Catalog\Setup\Patch\Data\AddAvailabilityLabelAttribute;

class ReadHandlerPlugin
{
    /**
     * @var ProductCustomOptionRepositoryInterface
     */
    private ProductCustomOptionRepositoryInterface $optionRepository;
    /**
     * @var OptionsProvider
     */
    private OptionsProvider $optionProvider;
    /**
     * @var Options
     */
    private Options $productOptions;
    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;
    /**
     * @var FilterEmulate
     */
    private FilterEmulate $filterEmulate;
    /**
     * @var Data
     */
    private $dataHelper;

    /**
     * @param ProductCustomOptionRepositoryInterface $optionRepository
     * @param OptionsProvider $optionProvider
     * @param Options $productOptions
     * @param LoggerInterface $logger
     * @param FilterEmulate $filterEmulate
     * @param Data $dataHelper
     */
    public function __construct(
        ProductCustomOptionRepositoryInterface $optionRepository,
        OptionsProvider                        $optionProvider,
        Options                                $productOptions,
        LoggerInterface                        $logger,
        FilterEmulate                          $filterEmulate,
        Data                                   $dataHelper
    )
    {
        $this->optionRepository = $optionRepository;
        $this->optionProvider = $optionProvider;
        $this->productOptions = $productOptions;
        $this->logger = $logger;
        $this->filterEmulate = $filterEmulate;
        $this->dataHelper = $dataHelper;
    }

    /**
     * @param ReadHandler $subject
     * @param $result
     * @param object $entity
     * @param array $arguments
     * @return mixed
     */
    public function afterExecute(ReadHandler $subject, $result, $entity, $arguments = [])
    {
        try {
            $options = $entity->getData('options');
            $dataOption = $this->optionProvider->getExtendedOptionsConfig($options);

            if ($dataOption != '' && $dataOption != null && $dataOption != '[]') {
                $entity->setOptions($dataOption);
            } else {
                $options = [];
                /** @var $entity \Magento\Catalog\Api\Data\ProductInterface */
                foreach ($this->optionRepository->getProductOptions($entity) as $option) {
                    $option->setProduct($entity);
                    $options[] = $option;
                }

                $entity->setOptions($options);
            }

            $description = $entity->getData('description') ?? '';
            $descriptionFilter = $this->filterEmulate->filter($description);
            $entity->setDescription($descriptionFilter);
            $resultLabelAttributeAvailability = $this->dataHelper->prepareAttributeAvailabilityLabel($entity);
            $entity->setData(AddAvailabilityLabelAttribute::ATTRIBUTE_AVAILABILITY_LABEL, $resultLabelAttributeAvailability);
        } catch (\Exception $exception) {
            $this->logger->error($exception->getMessage());
        }

        return $result;
    }
}
