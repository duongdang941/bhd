# marketing-electronique

#Choix 1
####1. Voile triangulaire
######Type: Configurable product
######Sku: Voile triangulaire
####2. Voile carrée
######Type: Configurable product
######Sku: Voile carrée
####3. Voile rectangle, losange, trapèze
######Type: Configurable product
######Sku: Voile rectangle, losange, trapèze
####4. Pack 2 voiles 22m² (côté gauche)
######Type: Bundle product
######Sku: Pack 2 voiles 22m² (côté gauche)
####5. Pack 2 voiles 22m² (côté droit)
######Type: Bundle product
######Sku: Pack 2 voiles 22m² (côté droit)
####6. Pack 2 voiles 20m² (côté gauche)
######Type: Bundle product
######Sku: Pack 2 voiles 20m² (côté gauche)
####7. Pack 2 voiles 20m² (côté droit)
######Type: Bundle product
######Sku: Pack 2 voiles 20m² (côté droit)
####8. Pack 3 voiles 15m²
######Type: Bundle product
######Sku: Pack 3 voiles 15m²
####9. Pack 3 voiles 24m²
######Type: Bundle product
######Sku: Pack 3 voiles 24m²

####API get configurable product (For Item 1 -> 3) 
```
{{magento_url}}/V1/configurable-products/(configurable-sku)/options/all
```

####API get bundle product (For Item 4 -> 9)
```
{{magento_url}}/V1/bundle-products/(bundle-sku)/options/all/
```

####API get accessories product (For Item 1 -> 9) . Get cross-sell item
```
{{magento_url}}/V1/products/(parent-product-sku)/links/crosssell
```

#Choix 2
####1. Dimensions de votre voile
######Type: Virtual product
######Sku: Dimensions de votre voile

####API get option product for this sku
```	
{{magento_url}}/V1/products/Dimensions de votre voile
```
####API get product option for _Choix 2.1.1.2 Mât hauteur réglable pour le côté pratique_
API get product list from category 54
```
{{magento_url}}/V1/categories/54/products
```
####API get product option for _Choix 2.1.1.3	Mât en inox pour une ambiance marine_
API get product list from category 55
```
{{magento_url}}/V1/categories/55/products
```
####API get product option for _Etape	Choix des accessoires complémentaires (not required)_
API get product list from category 56
```
{{magento_url}}/V1/categories/56/products
```

####2. Dimensions entre vos points d'accroche
######Type: Virtual product
######Sku: Dimensions entre vos points d'accroche

####API get option product for this sku
```
{{magento_url}}/V1/products/Dimensions entre vos points d'accroche
```
####API get product option for _Etape	Options d'accessoires_
API get product list from category 42
```
{{magento_url}}/V1/categories/42/products
```
####API get product option for _Choix 2.1.1.2	Mât hauteur réglable pour le côté pratique_
API get product list from category 54
```
{{magento_url}}/V1/categories/54/products
```
####API get product option for _Choix 2.1.1.3	Mât en inox pour une ambiance marine_
API get product list from category 55
```
{{magento_url}}/V1/categories/55/products
```
####API get product option for _Etape	Choix des accessoires complémentaires_
API get product list from category 56
```
{{magento_url}}/V1/categories/56/products
```

#Choix 3
####1. Contre 1 ou plusieurs murs
######Type: Virtual product
######Sku: Contre 1 ou plusieurs murs

####API get option product for this sku
```
{{magento_url}}/V1/products/Contre 1 ou plusieurs murs
```

####2. Au milieu de nulle part
######Type: Virtual product
######Sku: Au milieu de nulle part

####API get option product for this sku
```
{{magento_url}}/V1/products/Au milieu de nulle part
```
####API get product option for _Choix 2.1.1.2	Mât hauteur réglable pour le côté pratique_
API get product list from category 54
```
{{magento_url}}/V1/categories/54/products
```
####API get product option for _Choix 2.1.1.3	Mât en inox pour une ambiance marine_
API get product list from category 55
```
{{magento_url}}/V1/categories/55/products
```
####API get product option for _Etape	Choix des accessoires complémentaires (not required)_
API get product list from category 56
```
{{magento_url}}/V1/categories/56/products
```
